#!/usr/bin/env bash


# Generate sydney dataset
# echo "./launcher.sh genlabels objective_dstack ./Data/ADSBExchange/processed/Sydney_Far_all.json dstack/labelled_sydney_far_100p"
# ./launcher.sh genlabels objective_dstack ./Data/ADSBExchange/processed/Sydney_Far_all.json dstack/labelled_sydney_far_100p

# # Train sydney model
# echo "./launcher.sh train objective_cnn_vgg16 ./Data/labels_objective/dstack/labelled_sydney_far_100p/ vgg16_dstack_adelaide_far_combined"
# ./launcher.sh train objective_cnn_vgg16 ./Data/labels_objective/dstack/labelled_sydney_far_100p/ vgg16_dstack_sydney_far_combined

# # Test sydney data on Adelaide
# echo "./launcher.sh test objective_cnn_vgg16 ./Data/labels_objective/dstack/labelled_sydney_far_100p/ vgg16_dstack_adelaide_far_combined"
# ./launcher.sh test objective_cnn_vgg16 ./Data/labels_objective/dstack/labelled_sydney_far_100p/ vgg16_dstack_adelaide_far_combined

# Train VGG11 test
# echo "./launcher.sh train objective_cnn_vgg11 ./Data/labels_objective/graphic/labelled_adelaide_near_100p/ vgg11_graphic_adelaide_near_100p"
# ./launcher.sh train objective_cnn_vgg11 ./Data/labels_objective/graphic/labelled_adelaide_near_100p/ vgg11_graphic_adelaide_near_100p

# Train ResNet50 test
# echo "./launcher.sh train objective_cnn_resnet50 ./Data/labels_objective/graphic/labelled_adelaide_near_100p/ resnet50_graphic_adelaide_near_100p"
# ./launcher.sh train objective_cnn_resnet50 ./Data/labels_objective/graphic/labelled_adelaide_near_100p/ resnet50_graphic_adelaide_near_100p