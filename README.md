# README #

Code associated with honours project.

Liam Flaherty - a1687697

# Branches #

Project code is stored in the branch "master".

The thesis document is stored under the branch "thesis".

This is to allow commiting, pushing and pulling of thesis and codebase separately.