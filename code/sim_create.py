#!/usr/bin/env python3

import os
import sys
import json
import numpy as np
import random
import datetime

import tools.common as common
import tools.image_converter


def create_dict(data):
    output = []
    
    for sim_entry in data:
        label = sim_entry['label']
        waypoints = sim_entry['waypoints']
        name = sim_entry['name']

        raw_points = []

        t_max = waypoints[-1][3]
        counters = [0]*50

        for i in range(int(t_max)+1):
            for j, _ in enumerate(waypoints[:-1]):
                x_0 = np.array(waypoints[j])
                x_1 = np.array(waypoints[j+1])

                if x_0[3] <= i <= x_1[3]:
                    l = float(counters[j]) / (float(x_1[3]-x_0[3]))  # l increment at each time step
                    counters[j] += 1
                    new_entry = x_0 + l*(x_1-x_0)
                    raw_points.append(new_entry.tolist())
        
        # convert seconds to datetimes
        for i in range(len(raw_points)):
            dt = datetime.timedelta(seconds=raw_points[i][3])
            m, s = divmod(dt.seconds, 60)
            h, m = divmod(m, 60)
            dt = datetime.datetime(2018, 1, 1, h, m, s, dt.microseconds)
            raw_points[i][3] = dt
        
        new_entry = {}
        new_entry['label'] = label
        new_entry['name'] = name
        new_entry['points'] = raw_points
        output.append(new_entry)
    
    return output


def main(filename, output_dir):
    with open(filename) as data_file:
        data = json.load(data_file)

    output = create_dict(data)       
 
    
    # Make sure folders exist
    folders = [os.path.join(output_dir, '0/'), os.path.join(output_dir, '1/'), os.path.join(output_dir, '2/')]
    for folder in folders:
        if not os.path.exists(folder):
            os.makedirs(folder)
    
    for val in output:
        # mat = tools.image_converter.flight_to_image_graphic(common.IMAGE_FORMAT_BOUNDS, common.IMAGE_FORMAT_RESOLUTION, val['points'])
        mat = tools.image_converter.flight_to_image_tstack(val['points'], common.IMAGE_FORMAT_BOUNDS)
        tools.image_converter.save_image(mat, os.path.join(output_dir, str(val['label']), '{0}.png'.format(val['name'])))


if __name__ == '__main__':
    if len(sys.argv) != 3:
        print('Usage: sim_create.py sim_db.json output_folder')
    else:
        main(sys.argv[1], sys.argv[2])