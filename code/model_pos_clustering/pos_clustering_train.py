#!/usr/bin/env python3

import sys
import timeit
import datetime
import numpy as np
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans

import tools.kalman as kalman
from tools.plotting import plot_map, plot_scatter

def train(entries, airports):
    print('Entering trajectory clustering trainer...')

    # 1. Turning point identification
    S = {}   # dictionary of turning points
    Phi_C = 0.01
    phi_prev = 0

    for id, entry in entries.items():
        # First apply Kalman filter
        entry_array = np.array(entry)
        xhat, _ = kalman.run_kalman(entry_array)
        entry = xhat.tolist() 

        S[id] = []
        for i in range(len(entry)):
            if entry[i] == entry[0] or entry[i] == entry[-1]:
                continue
            
            # Get the t+1, t-1 elements
            entry_minus = entry[i-1]
            entry_plus = entry[i+1]
            x_minus, y_minus = entry_minus[1], entry_minus[0]
            x_plus, y_plus = entry_plus[1], entry_plus[0]

            # Calculate angle deviation
            phi_l = np.arctan2(x_plus - x_minus, y_plus - y_minus)

            if np.abs(phi_l - phi_prev) > Phi_C:
                S[id].append(entry[i])

            phi_prev = phi_l # store for next iteration
        
    # 2. turning point clutering - creation of waypoints
    S_flat = [item for sublist in S.values() for item in sublist]
    S_np = np.array([x[0:2] for x in S_flat])   # convert S to numpy array - need to remove datetime entry (NOTE: removed altitude as well)
    kmeans = KMeans(n_clusters=15, random_state=0).fit(S_np)
    centers = np.ndarray.tolist(kmeans.cluster_centers_)

    # TODO: GET STDDEV

    # 3. Converting a trajectory into sequence of way points
    # TODO: normally loop over the a/c here
    W_coords = {}
    for id, entry in S.items():
        W = []
        for (lat, long, _, _) in entry:
            # fit lat/long to
            x_test = np.array([[lat, long]])
            x = kmeans.predict(x_test)[0]   # get the cluster it belongs to (convert to just a number)
            if len(W) == 0:
                W.append(x)
            elif x != W[-1]:
                W.append(x)
        W_coords[id] = [centers[x] for x in W]   # convert to lat/long

    # plot_map(entry)
    print('Plotting...')
    plot_scatter2(entries, airports, S, centers, W_coords)

def plot_scatter2(ac_points, airports, S=None, centers=None, W=None, save_to=None):
    """
    Plots just as scatter graph without map in background
    :param ac_points: array of array of tuples - [(lat, long, alt, time), ...]
    :param airpots: array of tuples - [(lat, long), ...]
    If save_to set, save to file rather than displaying
    """
    for ac in ac_points.values():
        for (lat, long, _, _) in ac:
            plt.scatter(long, lat, s=1, marker='.', color='b')
            plt.plot(long, lat)

    if S is not None:
        for entry in S.values():
            for (lat, long, _, _) in entry:
                plt.scatter(long, lat, s=5, marker='+', color='c')
                plt.plot(long, lat)

    if centers is not None:
        for (lat, long) in centers:
            plt.scatter(long, lat, s=50, marker='^', color='g')
            plt.plot(long, lat)

    if W is not None:
        for W_item in W.values():
            lat = [x[0] for x in W_item]
            long = [x[1] for x in W_item]
            # plt.scatter(long, lat, s=0, marker=None, color='m')
            # plt.plot(long, lat)

            # add direction arrows
            for i in range(len(W_item)):
                if i == len(W_item)-1:
                    continue
                x, y = W_item[i][1], W_item[i][0]
                dx, dy= W_item[i+1][1]-x, W_item[i+1][0]-y

                plt.arrow(x, y, dx, dy, shape='full', lw=0, length_includes_head=True, head_width=.005, color='m')
    
    for (lat, long) in airports:
        plt.scatter(long, lat, s=50, marker='x', color='r')

    plt.xlabel('Longitude')
    plt.ylabel('Latitude')

    if save_to:
        plt.savefig(save_to)
    else:
        plt.show()