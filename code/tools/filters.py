# filter_* : apply a filter to a set of data
# filter_entry_* : apply a filter to a single entry of data 


def filter_entry_geographic_circle(entry, target_lat_long, radius):
    # first check lag and long entries
    if 'Lat' in entry and 'Long' in entry:
        test_lat_long = (entry['Lat'], entry['Long'])
        if within_lat_long(test_lat_long, target_lat_long, radius):
            return True

    # now check cos entries
    if 'Cos' in entry:
        for recording in entry['Cos'].values():
            test_lat_long = (recording['Lat'], recording['Long'])
            if within_lat_long(test_lat_long, target_lat_long, radius):
                return True
    
    return False


def filter_entry_geographic_rect(entry, target_bounds):
    """
    Filters into rectangular region.
    target_bounds is tuple of (lat_min, long_min, lat_max, long_max)
    """

    # first check lag and long entries
    if 'Lat' in entry and 'Long' in entry:
        test_lat_long = (entry['Lat'], entry['Long'])
        if target_bounds[0] <= test_lat_long[0] <= target_bounds[2] and \
            target_bounds[1] <= test_lat_long[1] <= target_bounds[3]:
            return True

    # now check cos entries
    if 'Cos' in entry:
        for recording in entry['Cos'].values():
            test_lat_long = (recording['Lat'], recording['Long'])
            if target_bounds[0] <= test_lat_long[0] <= target_bounds[2] and \
                target_bounds[1] <= test_lat_long[1] <= target_bounds[3]:
                return True

    return False


def filter_entry_country(entry, country):
    return 'Cou' in entry and entry['Cou'] == country


def filter_entry_sus(entry):
    return 'CallSus' in entry and entry['CallSus'] == False


def filter_entry_operator(entry, operators):
    """
    :param entry: hash entry
    :param operators: list of operators which are passed
    """
    return 'Op' in entry and entry['Op'] in operators


def filter_entry_man(entry, manufacturers):
    """
    :param entry: hash entry
    :param operators: list of manufacturers which are passed
    """
    return 'Man' in entry and entry['Man'] in manufacturers


def filter_entry_icao(entry, icao):
    """
    :param entry: hash entry
    :param icao: Icao to pass
    """
    return 'Icao' in entry and entry['Icao'] == icao


# test: tuple of (lat,long) of values to check
# target: tuple of (lag, long) 
def within_lat_long(test, target, radius):
    return (test[0] - target[0])**2 + (test[1] - target[1])**2 <= radius**2

