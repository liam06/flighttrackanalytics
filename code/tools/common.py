# Define constant data structures

COMMERCIAL_AIRLINE_ICAOS = [    # Allow these airlines through (only pass through commercial traffic)
    # AU Airlines:
    'VOZ',  # Virgin Airlines
    'QFA',  # Qantas
    'QLK', 'EAQ',  # QantasLink
    'JST',  # Jetstar
    'TGW',  # Tigerair Australia

    # AU Regional Airlines:
    # 'RXA',  # Region Express (REX)
    'OZW',  # Virgin Australia Regional Airlines
    'UTY',  # Alliance Airlines  (flies to Olymic Dam)

    # Foreign Airlines
    'QTR',  # Qatar
    'XAX',  # AirAsia X
    'MAS',  # Malaysia Airlines
    'SIA',  # Singapore Airlines
    'ALK',  # SriLankan Airlines
    'CSN',  # China Southern Airlines
    'JFI', 'FJI',  # Figi Airways
    'ANZ',  # Air New Zealand
    'GIA',  # Garuda Indonesia
    'ETD',  # Etihad Airways
    'UAE',  # Emirates Airline
    'THA',  # Thai Airways International
    'AIC',  # Air India
    'DAH',  # Air Algerie
    'CPA',  # Cathay Pacific Airways

    # Freight
    'TFR',  # Toll Priority
]

AIRPORTS = [
    (-34.947324, 138.533251),  # Adelaide airport
    (-34.796090, 138.635555),  # Parafield airport
    (-33.940936, 151.175273),  # Sydney Airport
]

# Matrix parameters
IMAGE_FORMAT_RESOLUTION = 2
IMAGE_ALT_MAX = 45000  # TODO: update this to a more appropriate value

# Parameters for Adelaide (Near):
# IMAGE_FORMAT_BOUNDS = (-35.6, 137.8, -34.0, 139.6)    # Adelaide (Near)
# IMAGE_TIME_MAX = 2000   # Seconds   # Use for Adelaide_Near
# IMAGE_DIST_MAX = 2.0   # Degrees  # Use for Adelaide_Near
# MAIN_AIPORT = AIRPORTS[0]

# Parameters for Adelaide (Far):
IMAGE_FORMAT_BOUNDS = (-38, 132, -27, 143)    # Adelaide (Far)
IMAGE_TIME_MAX = 3500      # Use for Adelaide_Far
IMAGE_DIST_MAX = 2.0   # Use for Adelaide_Far
MAIN_AIPORT = AIRPORTS[0]

# Parameters for Sydney (Near);
# IMAGE_FORMAT_BOUNDS = (-34.6, 150.1, -33, 151.9)   # Sydney (Near)
# IMAGE_TIME_MAX = 2000   # Seconds
# IMAGE_DIST_MAX = 2.0   # Degrees 
# MAIN_AIPORT = AIRPORTS[2]

# Parameters for Sydney (Far);
# IMAGE_FORMAT_BOUNDS = (-34.5, 147, -29.8, 155.9)   # Sydney (Far)
# IMAGE_TIME_MAX = 2000   # Seconds
# IMAGE_DIST_MAX = 2.0   # Degrees
# MAIN_AIPORT = AIRPORTS[2]

DATA_DATES = [
    '2018-03-05', 
    '2018-03-06', 
    '2018-03-07', 
    '2018-03-08', 
    '2018-03-09', 
    '2018-03-10',
    '2018-03-11',
    '2018-03-12',
    '2018-03-13',
    '2018-03-14',
    '2018-03-15',
]

BLACKLIST = [   # List of bad data to exclude (corrupted, etc.)
    "7C6D25_VOZ402",
    "7C77F9_QFA737",

]

ANOAMLIES = [   # Flights to be stored as an anomaly (for some manual data filtering)
    "7C1CA5_FD514", 
    "7C1C99_FD512", 
    "7C1CA2_FD531", 
    "7C3A38_LSA", 
    "7C148D_ECF", 
    "7C6A62_POL51", 
    "7C1C49_FD516", 
    "7C1C49_FD512", 
    "C05BB7_CGITA", 
    "7C1C99_FD538", 
    "7CF9CE_BLKT31", 
    "7C1C45_FD538", 
    "7CF9CF_BLKT30", 
    "7C07CD_NOCALL", 
    "7C3A38_RSCU55", 
    "7C7B64_YNQ", 
    "7CF9CD_BLKT21", 
    "7C582C_RPA", 
    "7C3929_LKR", 
    "7C1687_EQH", 
    "7C79BB_YBX", 
    "7C259F_HPT", 
    "7C4E42_PQS", 
    "7C168B_EQL", 
    "7C6A5E_POL52", 
    "7C6693_UJP", 
    "7C7B65_YNR", 
    "7C6A63_VAT", 
    "7C1C49_FD593", 
    "A27A8E_N259SS", 
    "7C6A5E_RSCU52", 
    "7C19C6_FD519", 
    "7C79B9_YBV", 
    "7C1CA2_FD512", 

]

MANUAL_LABELLINGS = {   # Manual labellings for cases that the automatic labelling got wrong
    "7C6B36_NOCALL": "arrival",
    "7C6D34_VOZ401_1": "departure",
    "7C089A_NOCALL": "passthrough"
}