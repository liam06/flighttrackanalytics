#!/usr/bin/env python3

import json
import sys
import os
import timeit
import datetime
import numpy as np

import tools.filters as filters
import tools.common as common


"""
    Abstracts the loading function to return arrays of data entries
    for aircraft positions and auxiliary datas
"""
def load(path):
    start_time = timeit.default_timer()    
    ac = load_json(common.DATA_DATES, path)   # ac contains hash
    print('Time to read and process:', timeit.default_timer() - start_time)
    # print('Max ac:', ac['area_data']['maxAc'])

    entries, aux_info = sort_entries(ac)
    return entries, aux_info


"""
    Loads the prossed .json data into a data structure
    dates :  Array of dates for raw data. Elements formatted as YYYY-MM-DD (e.g. 2018-03-20)
    """ 
def load_json(dates, path):
    data_all = {}

    print('Loading pre-processed data')

    files = []

    if os.path.isdir(path):
        for date in dates:
            files.append('{0}/{1}.json'.format(path, date))
    else:
        files.append(path)   # just the single file

    for file in files:
        with open(file) as data_file:
            data_n = json.load(data_file)
        
        # For now - filter single ICAO number
        # print('Loaded flights:')
        for id in data_n:
            if id == 'area_data':   # skip additional data
                data_all[id] = data_n[id]
                continue

            if id in common.BLACKLIST:  # skip "blacklisted" entries
                continue

            entry = data_n[id][0]   # all entries in the list should have the same Icao

            # determine whether to include entry - more filtering
            include = all([
                True,
                # Add further arbitrary filters here
                # filters.filter_entry_man(entry, ['Boeing', 'Airbus'])   # Only consider large commerical aircraft here (from Boeing/Airbus for now)
            ])
            # print('{0}\t- {1}'.format(id, 'Passed' if include else 'Removed'))

            data_all[id] = data_n[id]
        print('Loading: {0} complete'.format(file))

    print('Data entires loaded:', len(data_all))

    return data_all


def sort_entries(ac, min_ac_len=200):
    """
    Parameters
    ac:     Output from load_json
    min_ac_len: If the number of records for an aircraft is less than min_ac_len, ignore it

    Returns entries and aux_info
    entries: hash - key: Icao_Call, value: tuple of (lat, long, alt, datetime), sorted in time order
    aux_info: hash - key: Icao_Call, value: hash of mappings from various information (e.g. From, To, etc.) to values
    """
    out = {}
    aux_info = {}

    aux_fields = ['From', 'To', 'Call', 'Icao', 'Cou', 
                  'Man', 'Mdl', 'Op', 'OpIcao', 'EngMount', 
                  'EngType', 'Engines']

    for (id, entries) in ac.items():
        if id == 'area_data':    # custom field added by load_adsb_raw
            continue

        # create new hash entry for this iD
        out[id] = []

        for entry in entries:
            # populate the auxiliary info about this flight            
            aux_info[id] = {f: get_dict_safe(entry, f) for f in aux_fields}

            if 'Cos' not in entry:  # error check - no entries => skip this
                continue

            for (timestamp, position) in entry['Cos'].items():
                position_lat = position['Lat']
                position_long = position['Long']
                position_alt = position['Alt'] if position['Alt'] is not None else 0
                time = datetime.datetime.fromtimestamp(float(timestamp) / 1e3)

                out[id].append((position_lat, position_long, position_alt, time))
    
    for (id, entries) in out.items():
        entries_set = list(set(entries))        # remove duplicates
        out[id] = sorted(entries_set, key=lambda x: x[3])       # sort entries by time

    # Filter Altitude differences
    # for id in out:
    #     # Calculate altitude differences
    #     for i, j in enumerate(out[id][:-1]):
    #         entry_0 = out[id][i]
    #         entry_1 = out[id][i+1]

    #         diff = abs(entry_0[2]-entry_1[2])
    #         if diff > 10000:
    #             print(id, 'diff:', diff, 'entries:', entry_0[2], entry_1[2])
    #             break

    # Split different "flights" based on flight
    del_ids = set()
    new_items = {}   # store things temporarily here to avoid changing 'out'
    for id in out:
        # Calculate time difference
        entry_original = out[id]
        split_counter = 0
        i_0 = 0   # where the previous split started
        for i, j in enumerate(entry_original[:-1]):
            entry_0 = entry_original[i]
            entry_1 = entry_original[i+1]

            time_delta = entry_1[3] - entry_0[3]
            diff = time_delta.seconds + time_delta.microseconds/1e6
            if diff > 10*60:  # 10 minutes
                # Remove from the original dict
                del_ids.add(id)
                # create the new sub-array
                new_id = "%s_%d" % (id, split_counter)
                new_subarray = entry_original[i_0:i+1]
                new_items[new_id] = new_subarray
                aux_info[new_id] = aux_info[id]  # just copy the old info
                # update the split "pointer" and counter
                split_counter += 1
                i_0 = i+1
        if split_counter > 0:
            new_id = "%s_%d" % (id, split_counter)
            new_subarray = entry_original[i_0:]
            new_items[new_id] = new_subarray
            aux_info[new_id] = aux_info[id]  # just copy the old info
    # now modify 'out'
    for id in del_ids:
        del out[id]
        del aux_info[id]
    
    out.update(new_items)  # copy new items in

    # Make sure the tracks actually have meaningful data
    # (not just moving around the airport)
    del_ids = set()
    dist_threshold = 0.02
    for id, entry in out.items():
        entry_0 = np.array(entry[0][0:2])
        entry_n = np.array(entry[-1][0:2])
        dist = np.linalg.norm(entry_n-entry_0)
        if dist < dist_threshold:
            del_ids.add(id)
    for id in del_ids:
        del out[id]
        del aux_info[id]

    # Filter out flights with few position recordings
    remove_count = sum([1 for v in out.values() if len(v) < min_ac_len])
    print('Number of ac entries with less than %d entires: %d' % (min_ac_len, remove_count))
    out = {k: v for (k, v) in out.items() if len(v) >= min_ac_len}


    return out, aux_info


def get_dict_safe(dict, key):
    # Retrieves dict[key], but if key not in dict, returns None rather than an exception
    return dict[key] if key in dict else None

