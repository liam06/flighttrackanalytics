import numpy as np
import matplotlib.pyplot as plt


def kalman(z, n, Q, R, xhat_0=0.0, P_0=1.0):
    """
    Runs a Kalman Filter on the input
    Inputs and outputs are numpy vectors/matricess

    Args:
        z: Observations
        n: Length of observations
        Q: process variance
        R: measurement variance
        xhat_0: Initial estimate of state (default: 0.0)
        P_0: Initial estimate of error (default: 1.0)

    Returns:
        xhat: an estimate of state
        Pminus: a priori error estimate
    """
    # allocate space for arrays
    sz = z.shape # size of array
    xhat=np.zeros(sz)      # a posteri estimate of x
    P=np.zeros(sz)         # a posteri error estimate
    res=np.zeros(sz)       # residuals of xhat
    xhatminus=np.zeros(sz) # a priori estimate of x
    Pminus=np.zeros(sz)    # a priori error estimate
    K=np.zeros(sz)         # gain or blending factor

    # intial guesses
    xhat[0] = xhat_0
    P[0] = P_0

    for k in range(1,n):
        # time update
        xhatminus[k] = xhat[k-1]
        Pminus[k] = P[k-1]+Q

        # measurement update
        K[k] = Pminus[k]/( Pminus[k]+R )
        xhat[k] = xhatminus[k]+K[k]*(z[k]-xhatminus[k])
        P[k] = (1-K[k])*Pminus[k]
        res[k] = np.abs(z[k]-xhatminus[k])
    
    return xhat, Pminus, res


def run_kalman(entry):
    """
    Args:
        entry: numpy array of entries
    
    Returns:
        numpy array of estimated true state
    """

    x = entry[:,0]
    y = entry[:,1]
    z = entry[:,2]
    get_sec = np.vectorize(lambda dt: dt.seconds + dt.microseconds/1e6)
    t = entry[:,3] - entry[0,3]
    t = get_sec(t)
    z = np.column_stack((x,y,z,t))

    for t_i in t:
        pass
        # print(t_i)
    # print('------')
    
    # Kalman Filter paramters
    n = x.shape[0]
    Q = 1e-5
    R = 0.001

    xhat, _, res = kalman(z, n, Q, R, xhat_0=z[0])
    return xhat, res