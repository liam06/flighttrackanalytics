#!/usr/bin/env python3

import json
import sys
import os
import timeit
import numpy
from pprint import pprint
from datetime import datetime

import filters
import common

# PATH_RAW = './Data/ADSBExchange/raw'
# PATH_PROCESSED = './Data/ADSBExchange/processed'

def load_json(raw_dir, date):
    """
    Loads the raw .json data into a data structure and modifies structure for easier use
    Applies filters to reduce area
    Also adds an "area_data" entry to the hash with the following fields:
        maxAc : maximum number of aircraft during any minute of the day
        minAc : minimum number of aircraft during any minute of the day
        avgAc : average number of aircraft during any minute of the day
    
    raw_dir: Path of directory containing all YYYY-MM-DD-HHMMZ.json files
    """ 

    ac = {} 
    ac['area_data'] = {}

    ac_count = []  # array to contain the number of aircraft

    for i in range(24):
        for j in range(60):
            time = "%02d%02d" % (i,j)

            # path = '{0}/{1}/{1}-{2}Z.json'.format(PATH_RAW, date, time)
            path = os.path.join(raw_dir, '{0}-{1}Z.json'.format(date, time))
            print('Loading file:', path, end=' ')

            if not os.path.isfile(path):
                print('Doesn\'t exist')
                continue
            
            with open(path) as data_file:
                try:
                    data = json.load(data_file)
                except Exception as e:
                    print(' - ERROR PARSING JSON')
                    print('PARSE ERROR MESSAGE:', str(e))
                    print(repr(e))
                    continue   # move to the next data file if this one couldn't be loaded
                # print('JSON data data loaded...', end=' ')
            
            acList_json = data['acList']

            entry_count = 0
            current_ac_count = 0

            for entry in acList_json:
                Icao = entry['Icao']
                Call = entry['Call'] if 'Call' in entry else 'NOCALL'

                # re-sort Cos field (hash mapping time to lat/long/alt rather than list of those four)
                if 'Cos' in entry:
                    Cos = entry['Cos']
                    Cos_updated = {}   # key: time, value: dict of lat,long,alt

                    it = iter(Cos)
                    for x in it:
                        n_lat = x
                        n_long = next(it)
                        n_time = next(it)
                        n_alt = next(it)
                        n_date = str(datetime.fromtimestamp(n_time / 1e3))

                        updated_entry = {
                            "Lat": n_lat,
                            "Long": n_long,
                            "Alt": n_alt,
                            "Datetime": n_date,
                        }
                        Cos_updated[n_time] = updated_entry
                    
                    entry['Cos'] = Cos_updated

                # determine whether to include entry - initial filtering
                include = all([
                    # filters_basic.filter_entry_country(entry, 'Australia'),   # Limit to Australia for now
                    # filters.filter_entry_geographic(entry, (-34.9176, 138.5944), 0.6),   # Limit to Adelaide region
                    filters.filter_entry_geographic_rect(entry, common.IMAGE_FORMAT_BOUNDS),
                    filters.filter_entry_sus(entry),             # Remove data labelled as potentially incorrect
                    # filters.filter_entry_man(entry, ['Boeing', 'Airbus'])   # Only consider large commerical aircraft here (from Boeing/Airbus for now)
                ])

                if include:
                    entry_count += 1
                    entry_name = '{0}_{1}'.format(Icao, Call)   # use to uniquely identify aircraft and flights separately

                    if entry_name not in ac:
                        ac[entry_name] = [entry]
                        current_ac_count += 1
                    else:
                        ac[entry_name].append(entry)
            
            ac_count.append(current_ac_count)
            print(' - Done')
    
    ac['area_data']['minAc'] = min(ac_count)
    ac['area_data']['maxAc'] = max(ac_count)
    ac['area_data']['avgAc'] = numpy.mean(ac_count)

    print('Done')
    print('Entries loaded:', entry_count)
    print('A/c loaded:', len(ac))
    print('Area data:')
    pprint(ac['area_data'])

    return ac


if __name__ == '__main__':
    if len(sys.argv) != 3:
        print('Usage: load_adsb_raw.py raw_data_dir output_dir')
        print('E.g.: load_adsb_raw.py ./Data/ADSBExchange/raw/2018-03-05 ./Data/ADS-B Exchange/processed')
        print('\tWill create the file ./Data/ADSBExchange/processed/2018-03-05.json')
    else:
        raw_data = sys.argv[1]

        date = os.path.basename(os.path.basename(raw_data))
        if not date:
            date = os.path.basename(os.path.dirname(raw_data))

        output_data = os.path.join(sys.argv[2], '{0}.json'.format(date))

        print()
        print('Data source:', raw_data)
        print('Data output:', output_data)
        print()

        start_time = timeit.default_timer()    
        ac = load_json(raw_data, date)   # ac contains hash
        print('Time to read and process:', timeit.default_timer() - start_time)

        start_time = timeit.default_timer()
        # write ac json to file:
        with open(os.path.join(output_data), 'w') as outfile:
            json.dump(ac, outfile)
        print('Time to write json file:', timeit.default_timer() - start_time)

        # pprint(ac)
