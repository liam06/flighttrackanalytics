
import matplotlib.pyplot as plt
# from mpl_toolkits.basemap import Basemap


def plot_map(entries):  # plots (lat,long) tuples on the map
    print('Drawing map')
    # map=Basemap(projection="lcc",resolution="f",width=2E5,height=2E5,
    #                          lon_0=138.5944,lat_0=-34.9176,fix_aspect=False)  #-34.9176, 138.5944 is Adelaide
    # map.drawcountries(zorder=1,color="black", linewidth=1)
    # map.shadedrelief()
    # map.drawcoastlines(color="black",linewidth=1.2)
    # # map.drawrivers(linewidth=0.5,color="blue")
    # # map.drawmapboundary()

    # print('Adding aircraft')
    # for entry in entries:
    #     entry_lat, entry_long = entry[0], entry[1]
    #     map.scatter(entry_long, entry_lat, latlon=True)
    
    # plt.show()


def plot_scatter(ac_points, airports, S=None, centers=None, W=None, save_to=None):
    """
    Plots just as scatter graph without map in background
    :param ac_points: array of tuples - [(lat, long, alt, time), ...]
    :param airpots: array of tuples - [(lat, long), ...]
    If save_to set, save to file rather than displaying
    """
    for (lat, long, _, _) in ac_points:
        plt.scatter(long, lat, s=1, marker='.', color='b')
        plt.plot(long, lat)

    if S is not None:
        for (lat, long, _, _) in S:
            plt.scatter(long, lat, s=5, marker='+', color='c')
            plt.plot(long, lat)

    if centers is not None:
        for (lat, long) in centers:
            plt.scatter(long, lat, s=50, marker='^', color='g')
            plt.plot(long, lat)

    if W is not None:
        lat = [x[0] for x in W]
        long = [x[1] for x in W]
        # plt.scatter(long, lat, s=0, marker=None, color='m')
        # plt.plot(long, lat)

        # add direction arrows
        for i in range(len(W)):
            if i == len(W)-1:
                continue
            x, y = W[i][1], W[i][0]
            dx, dy= W[i+1][1]-x, W[i+1][0]-y

            plt.arrow(x, y, dx, dy, shape='full', lw=0, length_includes_head=True, head_width=.005, color='m')
    
    for (lat, long) in airports:
        plt.scatter(long, lat, s=50, marker='x', color='r')

    plt.xlabel('Longitude')
    plt.ylabel('Latitude')

    if save_to:
        plt.savefig(save_to)
    else:
        plt.show()