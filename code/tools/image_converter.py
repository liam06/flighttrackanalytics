# Functions for converting the ADS-B flight data into an image

import math
import numpy as np
from PIL import Image

import tools.common as common

def flight_to_image_graphic(bounds, dec_resolution, flight_array, interpolate=False):
    """
    Converts image data to a 2D matrix ("image-like"). Matrix values lie between 0 and 1

    bounds          tuple of (lat min, long min, lat max, long max)
    dec_resolution  Number of decimal places
    flight_array    Array of (lat,long,alt,time) items
    """
    
    width, height = get_mat_size(bounds, dec_resolution)

    mat = np.zeros((height, width, 3))

    entry_prev = None
    warned = False
    for entry in flight_array:
        _, _, _, time_0 = quantize_lat_long(bounds, dec_resolution, flight_array[0])
        lat_i, long_i, alt_i, time_i = quantize_lat_long(bounds, dec_resolution, entry)
        time_delta = time_i - time_0
        time_delta_sec = time_delta.seconds + time_delta.microseconds/1e6

        if not bounds[0] < entry[0] < bounds[2]:
            if not warned:
                # print('Lat {0} out of bounds: {1}'.format(entry[0], bounds))
                warned = True
            continue
        if not bounds[1] < entry[1] < bounds[3]:
            if not warned:
                # print('Long {0} out of bounds: {1}'.format(entry[1], bounds))
                warned = True
            continue

        try:
            mat[lat_i, long_i, 0] = alt_i
            mat[lat_i, long_i, 1] = time_delta_sec

            if interpolate:
                if entry_prev is not None:
                    lat_i0, long_i0, alt_i0, time_i0 = quantize_lat_long(bounds, dec_resolution, entry_prev)

                    p1 = np.array([lat_i0, long_i0, alt_i0, time_i0])
                    p2 = np.array([lat_i, long_i, alt_i, time_i])

                    # Compute linear fit between these two points:
                    for t in range(int(np.round(np.linalg.norm(p2-p1)))):
                        p_i = p1 + np.floor((t/np.linalg.norm(p2-p1))*(p2-p1))

                        mat[int(p_i[0]), int(p_i[1]), 0] = p_i[2]
                        mat[int(p_i[0]), int(p_i[1]), 1] = time_i0

                    # Reset start and end points (to ensure they're the same)
                    mat[int(p1[0]), int(p1[1]), 0] = p1[2]
                    mat[int(p2[0]), int(p2[1]), 0] = p2[2]
                    mat[int(p1[0]), int(p1[1]), 1] = p1[3]
                    mat[int(p2[0]), int(p2[1]), 1] = p2[3]

        except IndexError:
            print('WARNING: Flight data out of image bounds - {0}, {1} (indices {2}, {3})'.
                format(entry[0], entry[1], lat_i, long_i)
            )

        entry_prev = entry
    
    # Add airports
    for airport in common.AIRPORTS:
        airport_lat, airport_long, _, _ = quantize_lat_long(bounds, dec_resolution, (airport[0], airport[1], 0, 0))
        try:
            mat[int(airport_lat), int(airport_long), 2] = 1.0
        except IndexError:
            pass
    
    # scale matrix
    # scale altitude (channel 0)
    if (mat[:,:,0] > common.IMAGE_ALT_MAX).any():
        print('Flight data exceeds IMAGE_ALT_MAX')
    mat[:,:,0][mat[:,:,0] < 0] = 0    # clip "negative" altitudes
    mat[:,:,0][mat[:,:,0] > common.IMAGE_ALT_MAX] = common.IMAGE_ALT_MAX    # clip at mat altitude
    mat[:,:,0] = mat[:,:,0]/common.IMAGE_ALT_MAX

    # scale time (channel 1)
    if (mat[:,:,1] > common.IMAGE_TIME_MAX).any():
        print('Flight data exceeds IMAGE_TIME_MAX')
    mat[:,:,1][mat[:,:,1] > common.IMAGE_TIME_MAX] = common.IMAGE_TIME_MAX    # clip at mat time
    mat[:,:,1] = mat[:,:,1]/common.IMAGE_TIME_MAX

    return mat


# Converts the flight array to an image.
# 4 channels (lat,lon,alt,t) stacked from left to right and wrapped.
# Sorted w.r.t time
# Image size is size*size (input parameter), default is 80x80 (and has max size*size elements)
# Bounds are used to reduce quantization error when converted to image
def flight_to_image_tstack(flight_array, bounds, size=80):
    mat = np.zeros((size, size, 4))

    i = 0
    for _,entry in enumerate(flight_array):
        if i >= size*size:
            break

        _, _, _, time_0 = flight_array[0]
        lat_i, long_i, alt_i, time_i = entry

        # Make sure lat/long is within bounds
        if lat_i < bounds[0] or lat_i > bounds[2]:
            continue
        elif long_i < bounds[1] or long_i > bounds[3]:
            continue

        # Adjust time
        time_delta = time_i - time_0
        time_delta_sec = time_delta.seconds + time_delta.microseconds/1e6
        time_delta_sec = min(time_delta_sec, common.IMAGE_TIME_MAX)
        time_delta_sec /= common.IMAGE_TIME_MAX

        # Adjust lat/long/alt based on bounds (to be [0,1])
        lat_i = (lat_i - bounds[0])/(bounds[2]-bounds[0])
        long_i = (long_i - bounds[1])/(bounds[3]-bounds[1])
        alt_i = min(alt_i, common.IMAGE_ALT_MAX)
        alt_i = max(alt_i, 0)
        alt_i /= common.IMAGE_ALT_MAX

        # Finally, place everything in the matrix
        i_row = int(i/size)
        i_col = i%size
        i += 1
        mat[i_row, i_col, 0] = lat_i
        mat[i_row, i_col, 1] = long_i
        mat[i_row, i_col, 2] = alt_i
        mat[i_row, i_col, 3] = time_delta_sec
    
    return mat


# Same as tstack but:
# has 4 channels (dist from airport, angle from airport, alt, t)
# Sorts the flight by distance
def flight_to_image_dstack(flight_array, bounds, size=80):
    mat = np.zeros((size, size, 4))

    intermediate = []   # Stores (dist, angle, t), sorted by t at first

    for _, entry in enumerate(flight_array):
        _, _, _, time_0 = flight_array[0]
        lat_i, long_i, alt_i, time_i = entry

        # Make sure lat/long is within bounds
        if lat_i < bounds[0] or lat_i > bounds[2]:
            continue
        elif long_i < bounds[1] or long_i > bounds[3]:
            continue

        # Adjust time
        time_delta = time_i - time_0
        time_delta_sec = time_delta.seconds + time_delta.microseconds/1e6
        time_delta_sec = min(time_delta_sec, common.IMAGE_TIME_MAX)
        time_delta_sec /= common.IMAGE_TIME_MAX

        # TODO: very flaky, probably update this
        airport_lat, airport_long = common.MAIN_AIPORT
        
        dist = np.sqrt( (airport_lat-lat_i)**2 + (airport_long-long_i)**2 )
        dist = min(dist, common.IMAGE_DIST_MAX)
        dist /= common.IMAGE_DIST_MAX

        angle = np.arctan2(lat_i-airport_lat, long_i-airport_long)
        angle = (angle + np.pi) / (2*np.pi)

        alt_i = min(alt_i, common.IMAGE_ALT_MAX)
        alt_i = max(alt_i, 0)
        alt_i /= common.IMAGE_ALT_MAX

        new_entry = (dist, angle, alt_i, time_delta_sec)
        intermediate.append(new_entry)
    
    # Now sort by distance
    dsorted = sorted(intermediate, key=lambda e: e[0])

    for i, entry in enumerate(dsorted):
        if i == size*size:
            # Too big for input size
            break

        # Place everything in the matrix
        i_row = int(i/size)
        i_col = i%size
        mat[i_row, i_col, 0] = entry[0]
        mat[i_row, i_col, 1] = entry[1]
        mat[i_row, i_col, 2] = entry[2]
        mat[i_row, i_col, 3] = entry[3]
    
    return mat


def quantize_lat_long(bounds, dec_resolution, flight_entry):
    """
    Generate quantized lat/long to be added to the flight image matrix

    Inputs:
    bounds          
    dec_recolution  
    flight_entry    Array containing entries of (lat, long, alt, time)

    Outputs:
    tuple of (lat, long, alt, time)
    """
    lat_min, long_min, lat_max, long_max = bounds  # unpack tuple
    resolution = math.pow(10, -dec_resolution)

    lat_i, long_i, alt_i, time_i = flight_entry  # unpack tuple

    # round to specified resolution
    lat_i = round(lat_i, dec_resolution)
    long_i = round(long_i, dec_resolution)
    
    # offset from lat/long min and convert to int
    lat_i = int((lat_max - lat_i) / resolution)
    long_i = int((long_i - long_min) / resolution)

    return lat_i, long_i, alt_i, time_i


def get_mat_size(bounds, dec_resolution):
    """
    Gets the image size to be returned from flight_to_image

    Input:
    bounds          tuple of (lat min, long min, lat max, long max)
    dec_resolution  Number of decimal places

    Output:
    tuple of (width, height)
    """
    lat_min, long_min, lat_max, long_max = bounds  # unpack tuple
    resolution = math.pow(10, -dec_resolution)
    
    width = int(math.ceil((long_max - long_min) / resolution))
    height = int(math.ceil((lat_max - lat_min) / resolution))

    return width, height


def save_image(mat, filename):
    im = Image.fromarray((mat * 255).astype(np.uint8))
    im.save(filename)

