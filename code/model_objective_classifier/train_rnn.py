import torch
import torch.nn as nn
from torch.autograd import Variable
from torch.utils.data import Dataset
from torch.utils.data import DataLoader
import random
import time
import math
import numpy as np
import os
import json

from tools.load_adsb import load
from model_objective_classifier.objective_labeller import load_labels
from model_objective_classifier.rnn import Model, RNN
from model_objective_classifier.rnn_data_loader import FlightDataset


def load_data(dataset_dir, classes, num_training_rate):
    print('Loading data...')
    # Load flight data
    entries, aux_info = load(dataset_dir)

    # Load labels
    labels = load_labels(entries, aux_info, 'graphic/labelled_adelaide_far_100p')

    # Combine the two
    # category_flights_train = {
    #     0: [],
    #     1: [],
    #     2: [],
    # }
    # category_flights_test= {
    #     0: [],
    #     1: [],
    #     2: [],
    # }
    category_flights_train = {
        classes[0]: [],
        classes[1]: [],
        classes[2]: [],
    }
    category_flights_test= {
        classes[0]: [],
        classes[1]: [],
        classes[2]: [],
    }

    for (id, entry) in entries.items():
        if id not in labels:
            continue    # wasn't labelled, so skip

        label = labels[id]-1 # 'labels' is 1-index, whereas the other is 0-indexed

        choice = np.random.choice([0,1], p=[num_training_rate, 1-num_training_rate])
        if choice == 0:
            category_flights_train[classes[label]].append((id, entry))
        elif choice == 1:
            category_flights_test[classes[label]].append((id, entry))

    return category_flights_train, category_flights_test


def add_partial_data(dataset, classes):
    """
    Takes dataset from output of load_data and returns the same but with 25%, 50%, 75%, and 100% tracks
    """
    output = {
        classes[0]: [],
        classes[1]: [],
        classes[2]: [],
    }

    for category, cat_entries in dataset.items():
        for id, entry in cat_entries:
            for track_fraction in [0.25, 0.5, 0.75, 1.0]:
                # Create the partial entry:
                line = (id, entry[:int(track_fraction*len(entry))])
                output[category].append(line)
    
    return output



def flight_to_tensor(flight_entry):
    """
    flight_entry: tuple of (id, entry)
    """
    tensor = torch.zeros(len(flight_entry[1]), 1, 4)

    # Iterate through everything, assigning the 4 variables
    for i in range(len(flight_entry[1])):
        entry_i = flight_entry[1][i]   # tuples of (id,entry) so the [1] gets the entry
        entry_0 = flight_entry[1][0]

        tensor[i][0][0] = entry_i[0]   # Lat
        tensor[i][0][1] = entry_i[1]   # Long
        tensor[i][0][2] = entry_i[2]   # Alt
        time_delta = entry_i[3] - entry_0[3]   # t_i - t_0
        time_delta_sec = time_delta.seconds + time_delta.microseconds/1e6
        tensor[i][0][3] = time_delta_sec   # Time
    
    return tensor

    
def categoryFromOutput(output, all_categories):
    top_n, top_i = output.topk(1)
    category_i = top_i[0].item()
    return all_categories[category_i], category_i


def train_entry(category_tensor, line_tensor, rnn, learning_rate):
    # Set criterion
    criterion = nn.NLLLoss()
    # criterion = nn.CrossEntropyLoss()
    # criterion = torch.nn.MSELoss()

    # hidden = rnn.initHidden()

    rnn.zero_grad()

    # for i in range(line_tensor.size()[0]):
    #     output, hidden = rnn(line_tensor[i], hidden)
    output = rnn(line_tensor)

    print(output.shape)
    print(category_tensor.shape)
    print(category_tensor)
    print(category_tensor.type())
    print(output.type())
    loss = criterion(output, category_tensor)
    loss.backward()

    # Add parameters' gradients to their values, multiplied by learning rate
    for p in rnn.parameters():
        p.data.add_(-learning_rate, p.grad.data)

    return output, loss.item()


def evaluate_entry(category_tensor, line_tensor, rnn, learning_rate):
    # hidden = rnn.initHidden()

    # for i in range(line_tensor.size()[0]):
    #     output, hidden = rnn(line_tensor[i], hidden)
    output = rnn(line_tensor)

    return output


def randomChoice(l):
    return l[random.randint(0, len(l) - 1)]


def randomTrainingExample(all_categories, category_flights):
    category = randomChoice(all_categories)
    line = randomChoice(category_flights[category])

    category_tensor = torch.Tensor([0,0,0])
    # category_tensor = torch.tensor([all_categories.index(category)], dtype=torch.long)
    category_tensor[all_categories.index(category)] = 1
    category_tensor = category_tensor.unsqueeze(0)

    line_tensor = flight_to_tensor(line)
    return category, line, category_tensor, line_tensor


def timeSince(since):
    now = time.time()
    s = now - since
    m = math.floor(s / 60)
    s -= m * 60
    return '%dm %ds' % (m, s)


def train(dataset_dir, model_dir):
    if not os.path.exists(model_dir):
        os.makedirs(model_dir)
    filename = os.path.join(model_dir, 'objective_rnn.dat')

    # load data
    classes = ('arrival', 'departure', 'passthrough')

    num_training_rate = 0.6
    category_flights_train, category_flights_test = load_data(dataset_dir, classes, num_training_rate)

    # Add partial tracks
    # category_flights_train = add_partial_data(category_flights_train, classes)
    # category_flights_test = add_partial_data(category_flights_test, classes)

    # Define Network
    n_hidden = 128
    n_input = 4    # lat, lon, alt, time
    n_output = 3   # labels
    rnn = RNN(n_input, n_hidden, n_output)

    # Train
    learning_rate = 1e-7
    
    n_iters = 20 #1000000
    print_every = 5000
    plot_every = 1000

    # Keep track of losses for plotting
    current_loss = 0
    all_losses = []

    start = time.time()

    iter = 0
    for i in range(n_iters):
        for category, cat_entries in category_flights_train.items():
            for id, entry in cat_entries:
                line = (id, entry)
                line_tensor = flight_to_tensor(line)
                category, line, category_tensor, line_tensor = randomTrainingExample(classes, category_flights_train)
                output, loss = train_entry(category_tensor, line_tensor, rnn, learning_rate)
                current_loss += loss

                # Print iter number, loss, name and guess
                iter += 1
                if iter % print_every == 0:
                    guess, guess_i = categoryFromOutput(output, classes)
                    correct = '✓' if guess == category else '✗ (%s)' % category
                    print('%d %d%% (%s) %.4f %s %s' % (iter, (i+1) / n_iters * 100, timeSince(start), loss, guess, correct))

                # Add current loss avg to list of losses
                if iter % plot_every == 0:
                    all_losses.append(current_loss / plot_every)
                    current_loss = 0

    # Save RNN graph to disk
    print('Saving CNN to disk -', filename)
    torch.save(rnn.state_dict(), filename)
    
    # Now test training examples
    num_correct = { classes[0]: 0, classes[1]: 0, classes[2]: 0, "all": 0 }
    num_total = { classes[0]: 0, classes[1]: 0, classes[2]: 0, "all": 0 }
    labels_correct, labels_incorrect = {}, {}
    for category, cat_entries in category_flights_test.items():
        for id, entry in cat_entries:
            line = (id, entry)
            category_tensor = torch.tensor([classes.index(category)], dtype=torch.long)
            line_tensor = flight_to_tensor(line)
            # category, line, category_tensor, line_tensor = randomTrainingExample(classes, category_flights_train)
            output = evaluate_entry(category_tensor, line_tensor, rnn, learning_rate)

            guess, guess_i = categoryFromOutput(output, classes)
            if guess == category:
                num_correct[category] += 1
                num_correct["all"] += 1
                labels_correct[id] = {}
                labels_correct[id]['Label'] = category
                labels_correct[id]['Predcited'] = guess
                labels_correct[id]['score0'] = output[0][0].item()
                labels_correct[id]['score1'] = output[0][1].item()
                labels_correct[id]['score2'] = output[0][2].item()
            else:
                labels_incorrect[id] = {}
                labels_incorrect[id]['Label'] = category
                labels_incorrect[id]['Predcited'] = guess
                labels_incorrect[id]['score0'] = output[0][0].item()
                labels_incorrect[id]['score1'] = output[0][1].item()
                labels_incorrect[id]['score2'] = output[0][2].item()

            # Update totals
            num_total[category] += 1
            num_total["all"] += 1
    

    print('Rate correct:')
    for category in num_total.keys():
        print(category, ' - # correct: ', num_correct[category], '/', num_total[category], ' - % Correct: ', 100.0*num_correct[category]/num_total[category])
    
    # save labels_incorrect to file
    with open(os.path.join(model_dir, 'objective_errors.json'), 'w') as outfile:
        json.dump(labels_incorrect, outfile, indent=4, sort_keys=True)
    with open(os.path.join(model_dir, 'objective_correct.json'), 'w') as outfile:
        json.dump(labels_correct, outfile, indent=4, sort_keys=True)


def train_new(dataset_dir, model_dir):
    print()
    print('RNN: Starting')
    # load data
    classes = ('arrival', 'departure', 'passthrough')

    print('RNN: Loading data from file')
    num_training_rate = 0.6
    category_flights_train, category_flights_test = load_data(dataset_dir, classes, num_training_rate)

    print('RNN: Loading dataset')
    dataset = FlightDataset(category_flights_train)
    train_loader = DataLoader(dataset=dataset, batch_size=1, shuffle=False, num_workers=1)


    if not os.path.exists(model_dir):
        os.makedirs(model_dir)
    filename = os.path.join(model_dir, 'objective_rnn.dat')

    print('RNN: Creating model')
    input_size = 4
    hidden_size = 32
    output_size = 3

    try:
        model = torch.load(filename)
    except:
        model = Model(input_size, hidden_size, output_size)

    criterion = torch.nn.MSELoss()
    optimizer = torch.optim.Adam(model.parameters(), lr=0.001)

    print('RNN: Beginning train')
    epochs = 1

    for epoch in range(epochs):
        predictions = []
        correct_values = []

        for i, data in enumerate(train_loader):
            xs, ys, ids = data
            xs, ys = Variable(xs), Variable(ys)
            print(xs.shape)

            y_pred = model(xs)
            loss = criterion(y_pred, ys)
            optimizer.zero_grad()
            loss.backward(retain_graph=True)
            torch.nn.utils.clip_grad_norm(model.parameters(), 0.5)
            optimizer.step()

            predictions.append(y_pred.cpu().data.numpy().ravel())
            correct_values.append(ys.cpu().data.numpy().ravel())


    torch.save(model, filename)