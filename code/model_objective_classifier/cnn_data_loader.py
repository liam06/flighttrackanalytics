import torch
import torchvision
import torchvision.transforms as transforms
import torchvision.datasets as datasets
import os

class ImageFolderPath(datasets.ImageFolder):
    """ ImageFolder but also retains the image paths
    """

    def __getitem__(self, index):
        """
        Args:
            index (int): Index

        Returns:
            tuple: (image, target) where target is class_index of the target class.
        """
        path, target = self.imgs[index]
        img = self.loader(path)
        if self.transform is not None:
            img = self.transform(img)
        if self.target_transform is not None:
            target = self.target_transform(target)

        return img, target, path


def load_data(data_dir):
    dsets = {x: ImageFolderPath(os.path.join(data_dir, x), transform=transforms.ToTensor())
             for x in ['matrix_train', 'matrix_test']}
    dset_loaders = {x: torch.utils.data.DataLoader(dsets[x], batch_size=4,
                                                   shuffle=True)
                    for x in ['matrix_train', 'matrix_test']}
    dset_sizes = {x: len(dsets[x]) for x in ['matrix_train', 'matrix_test']}
    dset_classes = dsets['matrix_train'].classes

    print('-- Loaded sizes:', dset_sizes)
    print('-- Loaded classes:', dset_classes)
    
    return dset_loaders['matrix_train'], dset_loaders['matrix_test'] 