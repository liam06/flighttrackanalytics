from PIL import Image
import numpy as np
import os
import random
import json
from sklearn import svm
from sklearn.externals import joblib

from tools.load_adsb import load
import tools.common as common

def load_data(data_dir, set):
    X = []
    y = []


    # load labels
    labels_filename = './Data/labels_objective/graphic/labelled_adelaide_near_100p/labelled.json'
    with open(labels_filename) as data_file:
        labels = json.load(data_file)

    # load items
    entries, aux_info = load(data_dir)
    for id, data in entries.items():
        if id in labels:
            t_0 = data[0][3]
            data_new = np.zeros(6400*4)

            count = 0
            for i, sample in enumerate(data): 
                # Make sure lat/long is within bounds
                if sample[0] < common.IMAGE_FORMAT_BOUNDS[0] or sample[0] > common.IMAGE_FORMAT_BOUNDS[2]:
                    continue
                elif sample[1] < common.IMAGE_FORMAT_BOUNDS[1] or sample[1] > common.IMAGE_FORMAT_BOUNDS[3]:
                    continue

                time_delta = sample[3] - t_0
                time_delta_sec = time_delta.seconds + time_delta.microseconds/1e6
                sample_data = np.array([sample[0], sample[1], sample[2], time_delta_sec])
                data_new[4*i:4*i+4] = sample_data               

                count += 1
                if count == 6400:
                    break
                
                if i >= 1*len(data):
                    break

            # Add to data
            X.append(data_new)
            y.append(labels[id]-1)
    

    # randomize order
    X, y = combined_shuffle(X, y)

    return X, y


def load_image(filename):
    img = Image.open( filename )
    img.load()
    data = np.asarray( img, dtype="int8" )
    return data

def combined_shuffle(a, b):
    combined = list(zip(a, b))
    random.shuffle(combined)

    a[:], b[:] = zip(*combined)
    return a, b


def run(dataset_dir, model_dir):
    # load data
    print('Loading data')
    X_test, y_test = load_data(dataset_dir, 'matrix_test')
    
    # Load model
    clf = joblib.load(os.path.join(model_dir, 'model.dat'))

    print('Testing SVM')
    pred = clf.predict(X_test)
    results = np.array(y_test) == np.array(pred)
    accuracy = np.sum(results)/results.size
    print('Accuracy: ', accuracy)