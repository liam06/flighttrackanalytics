import torch
import torch.nn as nn
import random
import time
import math
import numpy as np
import os
import json
from pathlib import Path

from tools.load_adsb import load
from model_objective_classifier.objective_labeller import load_labels
from model_objective_classifier.rnn import RNN


def load_data(dataset_dir, classes, num_training_rate):
    print('Loading data...')
    # Load flight data
    entries, aux_info = load(dataset_dir)

    # Load labels
    labels = load_labels(entries, aux_info, 'labelled_adelaide_near_100p')

    # Combine the two
    category_flights_train = {
        classes[0]: [],
        classes[1]: [],
        classes[2]: [],
    }
    category_flights_test= {
        classes[0]: [],
        classes[1]: [],
        classes[2]: [],
    }

    for (id, entry) in entries.items():
        if id not in labels:
            continue    # wasn't labelled, so skip

        label = classes[labels[id]-1] # 'labels' is 1-index, whereas the other is 0-indexed

        choice = np.random.choice([0,1], p=[num_training_rate, 1-num_training_rate])
        if choice == 0:
            category_flights_train[label].append((id, entry))
        elif choice == 1:
            category_flights_test[label].append((id, entry))

    return category_flights_train, category_flights_test


def flight_to_tensor(flight_entry):
    """
    flight_entry: tuple of (id, entry)
    """
    tensor = torch.zeros(len(flight_entry), 1, 4)

    # Iterate through everything, assigning the 4 variables
    for i in range(len(flight_entry)):
        entry_i = flight_entry[1][i]
        entry_0 = flight_entry[1][0]

        tensor[i][0][0] = entry_i[0]   # Lat
        tensor[i][0][1] = entry_i[1]   # Long
        tensor[i][0][2] = entry_i[2]   # Alt
        time_delta = entry_i[3] - entry_0[3]   # t_i - t_0
        time_delta_sec = time_delta.seconds + time_delta.microseconds/1e6
        tensor[i][0][3] = time_delta_sec   # Time
    
    return tensor

    
def categoryFromOutput(output, all_categories):
    top_n, top_i = output.topk(1)
    category_i = top_i[0].item()
    return all_categories[category_i], category_i


def evaluate_entry(category_tensor, line_tensor, rnn, learning_rate):
    # Set criterion
    criterion = nn.NLLLoss()

    hidden = rnn.initHidden()

    rnn.zero_grad()

    for i in range(line_tensor.size()[0]):
        output, hidden = rnn(line_tensor[i], hidden)

    # Why do I need this remaining stuff?
    loss = criterion(output, category_tensor)
    loss.backward()

    # Add parameters' gradients to their values, multiplied by learning rate
    for p in rnn.parameters():
        p.data.add_(-learning_rate, p.grad.data)

    return output


def run(dataset_dir, model_dir):
    if not os.path.exists(model_dir):
        os.makedirs(model_dir)
    filename = os.path.join(model_dir, 'objective_rnn.dat')

    # load data
    classes = ('arrival', 'departure', 'passthrough')

    num_training_rate = 0.6
    _, category_flights_test = load_data(dataset_dir, classes, num_training_rate)

    # Define Network
    n_hidden = 128
    n_input = 4    # lat, lon, alt, time
    n_output = 3   # labels
    rnn = RNN(n_input, n_hidden, n_output)

    learning_rate = 1e-7

    # Load model
    if not Path(filename).exists():
        print('ERROR: could not find network:', filename)
        return
    rnn.load_state_dict(torch.load(filename))


    # Now test training examples
    num_correct = { classes[0]: 0, classes[1]: 0, classes[2]: 0, "all": 0 }
    num_total = { classes[0]: 0, classes[1]: 0, classes[2]: 0, "all": 0 }
    labels_correct, labels_incorrect = {}, {}
    for category, cat_entries in category_flights_test.items():
        for id, entry in cat_entries:
            line = (id, entry)
            category_tensor = torch.tensor([classes.index(category)], dtype=torch.long)
            line_tensor = flight_to_tensor(line)
            # category, line, category_tensor, line_tensor = randomTrainingExample(classes, category_flights_train)
            output = evaluate_entry(category_tensor, line_tensor, rnn, learning_rate)

            guess, guess_i = categoryFromOutput(output, classes)
            if guess == category:
                num_correct[category] += 1
                num_correct["all"] += 1
                labels_correct[id] = {}
                labels_correct[id]['Label'] = category
                labels_correct[id]['Predcited'] = guess
                labels_correct[id]['score0'] = output[0][0].item()
                labels_correct[id]['score1'] = output[0][1].item()
                labels_correct[id]['score2'] = output[0][2].item()
            else:
                labels_incorrect[id] = {}
                labels_incorrect[id]['Label'] = category
                labels_incorrect[id]['Predcited'] = guess
                labels_incorrect[id]['score0'] = output[0][0].item()
                labels_incorrect[id]['score1'] = output[0][1].item()
                labels_incorrect[id]['score2'] = output[0][2].item()

            # Update totals
            num_total[category] += 1
            num_total["all"] += 1
    

    print('Rate correct:')
    for category in num_total.keys():
        print(category, ' - # correct: ', num_correct[category], '/', num_total[category], ' - % Correct: ', 100.0*num_correct[category]/num_total[category])
    
    # save labels_incorrect to file
    with open(os.path.join(model_dir, 'objective_errors.json'), 'w') as outfile:
        json.dump(labels_incorrect, outfile, indent=4, sort_keys=True)
    with open(os.path.join(model_dir, 'objective_correct.json'), 'w') as outfile:
        json.dump(labels_correct, outfile, indent=4, sort_keys=True)