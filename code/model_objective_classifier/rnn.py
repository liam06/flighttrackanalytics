import torch
import torch.nn as nn

from torch.autograd import Variable


class RNN(nn.Module):
    def __init__(self, input_size, hidden_size, output_size):
        super(RNN, self).__init__()

        self.rnn = torch.nn.RNN(input_size, hidden_size,
                                num_layers=1, batch_first=True)
        self.h_0 = self.initHidden(hidden_size)

        self.linear = torch.nn.Linear(hidden_size, output_size)


    def forward(self, input):
        # for i in range(input.size()[0]):
        # print(input.shape)
        # print(input)
        for i in range(input.shape[0]):
            x = input[i].unsqueeze(0)
            output, self.h_0 = self.rnn(x, self.h_0)
        output = self.linear(output)
        return output

    def initHidden(self, hidden_size):
        return torch.zeros(1, 1, hidden_size)


class Model(torch.nn.Module):

    def __init__(self, input_size, rnn_hidden_size, output_size):

        super(Model, self).__init__()

        self.rnn = torch.nn.RNN(input_size, rnn_hidden_size,
                                num_layers=2, nonlinearity='relu',
                                batch_first=True)
        self.h_0 = self.initialize_hidden(rnn_hidden_size)

        self.linear = torch.nn.Linear(rnn_hidden_size, output_size)

    def forward(self, x):
        x = x.unsqueeze(0)
        self.rnn.flatten_parameters()
        out, self.h_0 = self.rnn(x, self.h_0)

        out = self.linear(out)

        # third_output = self.relu(self.linear3(second_output))
        # fourth_output = self.relu(self.linear4(third_output))
        # output = self.rnn(lineared_output)
        # output = self.dropout(output)
        return out

    def initialize_hidden(self, rnn_hidden_size):
        # n_layers * n_directions, batch_size, rnn_hidden_size
        return Variable(torch.randn(2, 1, rnn_hidden_size),
                        requires_grad=True)