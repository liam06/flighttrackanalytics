import datetime
import numpy as np
from sklearn import svm
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt

import model_objective_classifier.objective_labeller as labeller
import model_objective_classifier.cnn as objective_cnn
import tools.image_converter
import tools.common as common


INPUT_FORMAT_IMAGE = 0
INPUT_FORMAT_DIRECT = 1
ALGORITHM_SVM = 0
ALGORITHM_CNN = 1


# settings
input_format = INPUT_FORMAT_IMAGE
learning_algorithm = ALGORITHM_CNN


def run(entries_training, aux_info_training):
    # get labels
    print('Getting labels...')
    labels = labeller.load_labels(entries_training, aux_info_training)

    # format input
    print('Creating input format...')
    X, y = format_input(entries_training, aux_info_training, labels)

    # split into training and test data
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)
    print('Training data length:', X_train.shape[0])
    print('Test data length:', X_test.shape[0])

    # train
    print()
    print('Training...')
    model = train(X_train, y_train)

    # test
    print()
    print('Testing...')
    y_predict = predict(model, X_test)
    num_correct = np.sum(y_predict == y_test)
    num_incorrect = np.sum(y_predict != y_test)

    print()
    print('Results:')
    print('Predict correct:', num_correct)
    print('Predict incorrect:', num_incorrect)
    print('Rate correct:', num_correct/(num_correct+num_incorrect))

    print()
    print('Objective classifier done')

    # Test - TODO: remove
    return
    print('Running extra test...')
    count1, count2, count = 0, 0, 0
    ac_class1, ac_class2 = {}, {}
    pred_class1, pred_class2 = {}, {}
    for id, entries in entries_training.items():
        if id not in labels:  # TODO: why does this matter??
            continue
        
        count += 1
        
        if labels[id] == 1:
            count1 += 1
            if count1 > 4:
                continue
            ac_class1[id] = entries
            print('ac_class 1:', id)
        elif labels[id] == 2:
            count2 += 1
            if count2 > 4:
                continue
            ac_class2[id] = entries 
            print('ac_class 2:', id)       

        # predict the label now
        X_i = X[count,:].reshape(1, -1)
        predict_class = predict(model, X_i)

        if predict_class == 1:
            pred_class1[id] = entries
        elif predict_class == 2:
            pred_class2[id] = entries
        else:
            print('?? Predicted {0}'.format(predict_class))


    # Display all these elements
    print('Plotting extra test...')
    tmp_plot(ac_class1, ac_class2, common.AIRPORTS)  # display labels
    tmp_plot(pred_class1, pred_class2, common.AIRPORTS)  # display predicitions


def tmp_plot(ac_points1, ac_points2, airports):
    for ac in ac_points1.values():
        for (lat, long, _, _) in ac:
            plt.scatter(long, lat, s=1, marker='.', color='g')
            plt.plot(long, lat)
    
    for ac in ac_points2.values():
        for (lat, long, _, _) in ac:
            plt.scatter(long, lat, s=1, marker='.', color='b')
            plt.plot(long, lat)

    for (lat, long) in airports:
        plt.scatter(long, lat, s=50, marker='x', color='r')

    plt.xlabel('Longitude')
    plt.ylabel('Latitude')
    plt.show()


def format_input(entries_training, aux_info_training, labels):
    n = min(len(entries_training), len(labels))
    y = np.zeros(n)

    if input_format == INPUT_FORMAT_IMAGE:
        w,h = tools.image_converter.get_mat_size(common.IMAGE_FORMAT_BOUNDS, common.IMAGE_FORMAT_RESOLUTION)
        if learning_algorithm == ALGORITHM_SVM:
            X = np.zeros((n, w*h*3))  # for "image" input
        elif learning_algorithm == ALGORITHM_CNN:
            X = np.zeros((n, max(w, h), max(w, h), 3))  # for "image" input
    elif input_format == INPUT_FORMAT_DIRECT:
        X = np.zeros((n, 4*max([len(entry) for entry in entries_training.values()])))   # for "direct" input

    i = -1
    for id, entries in entries_training.items():
        if id not in labels:
            continue

        i += 1

        if input_format == INPUT_FORMAT_IMAGE:
            # "image" input
            X_i = tools.image_converter.flight_to_image(common.IMAGE_FORMAT_BOUNDS, common.IMAGE_FORMAT_RESOLUTION, entries)
            X_i = np.array(X_i)

            if learning_algorithm == ALGORITHM_SVM:
                X_i = X_i.flatten()
                X[i,:] = X_i
            elif learning_algorithm == ALGORITHM_CNN:
                X[i,:h,:w,:] = X_i

        elif input_format == INPUT_FORMAT_DIRECT:
            # "direct" input
            t0 = entries[0][3]
            X_i = np.zeros((len(entries),4))
            for j in range(len(entries)):   # for each j recordings of (lat,lon,alt,time)
                X_i[j, 0:3] = np.array(entries[j][0:3])
                X_i[j, 3] = (entries[j][3] - t0).total_seconds()

            X_i = X_i.flatten()
            X[i,:X_i.shape[0]] = X_i

        y[i] = labels[id]
    
    return X, y


def train(X_train, y_train):
    if learning_algorithm == ALGORITHM_SVM:
        model = svm.SVC()
        model.fit(X_train, y_train)
    elif learning_algorithm == ALGORITHM_CNN:
        objective_cnn.train(X_train, y_train)
        model = None
    else:
        model = None
    return model


def predict(model, X_test):
    if learning_algorithm == ALGORITHM_SVM:
        return model.predict(X_test)
    elif learning_algorithm == ALGORITHM_CNN:
        print('Not implemented')
        return None
    else:
        return None
