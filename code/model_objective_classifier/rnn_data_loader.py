import torch
import torch.nn as nn
from torch.utils.data import Dataset
import random
import time
import math
import numpy as np
import os
import json

from tools.load_adsb import load
from model_objective_classifier.objective_labeller import load_labels
import tools.common as common

class FlightDataset(Dataset):

    def __init__(self, data):
        # data: key: label, value: list of tuples (id, [entries of [lat,long,alt,time])

        x_data = []
        y_data = []
        ids = []

        
        for label, entries in data.items():
            for id, flight in entries:
                t_0 = flight[0][3]
                num_items = 6400 # required length of flight_new
                count = 0
                flight_new = []

                for lat,long,alt,time in flight:
                    # Make sure lat/long is within bounds
                    if lat < common.IMAGE_FORMAT_BOUNDS[0] or lat > common.IMAGE_FORMAT_BOUNDS[2]:
                        continue
                    elif long < common.IMAGE_FORMAT_BOUNDS[1] or long > common.IMAGE_FORMAT_BOUNDS[3]:
                        continue

                    time_delta = time - t_0
                    time_delta_sec = time_delta.seconds + time_delta.microseconds/1e6
                    sample_data = (lat,long,alt,time_delta_sec)
                    flight_new.append(sample_data)

                    count += 1
                    if count == num_items:
                        break
                
                if count < num_items:
                    diff = num_items-count
                    for _ in range(diff):
                        flight_new.append([0.0, 0.0, 0.0, 0.0])
                x_data.append(flight_new)
                y_data.append(label-1)
                ids.append(id)
                    

        # self.x_data = np.array(self.x_data)
        # self.y_data = np.array(self.y_data)

        # """ convert to torch """
        # self.x_data = torch.from_numpy(self.x_data)
        # self.y_data = torch.from_numpy(self.y_data)

        self.x_data = []
        self.y_data = []
        self.ids = []
        for i in range(len(ids)):
            self.ids.append(ids[i])

            x_np = np.array(x_data[i])
            y_np = np.array(y_data[i])
            self.x_data.append(torch.from_numpy(x_np))
            self.y_data.append(torch.from_numpy(y_np))

        self.x_data = torch.stack(self.x_data).float()
        self.y_data = torch.stack(self.y_data).float()
        self.len = self.y_data.shape[0]


    def __getitem__(self, index):
        return self.x_data[index], self.y_data[index], self.ids[index]

    def __len__(self):
        return self.len