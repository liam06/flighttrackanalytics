import sys
import os
import timeit
import datetime
import json
import numpy as np
import operator
import random
from pprint import pprint

from tools.plotting import plot_map, plot_scatter
import tools.common as common
import tools.image_converter


LABEL_CLASSES = {
    "arrival": 1,  # carrying people vs cargo?, large vs light commerical (e.g. Rex)?
    "departure": 2,
    "passthrough": 3,
}


def load_labels(entries, aux_info, source):
    """
    Loads existing labels from disk 
    (need to call create_labels at some point to create/update the file)
    """
    labelled = None
    path = './Data/labels_objective/{0}/'.format(source)

    if os.path.exists(os.path.join(path, 'labelled.json')):
        with open(os.path.join(path, 'labelled.json')) as data_file:
            labelled = json.load(data_file)
    else:
        print('Labels file does not exist, generating now...')
        labelled = create_labels(entries, aux_info, source)

    # Display stats of loaded labels
    print('Loaded labels:')
    print_label_count(labelled)
    print()
    
    return labelled

def create_labels(entries, aux_info, save_to, image_method, save_images=True, save_plots=False):
    labelled = {}
    unlabelled = {}
    path = './Data/labels_objective/{0}/'.format(save_to)

    # Load labelled from disk
    if os.path.exists(os.path.join(path, 'labelled.json')):
        with open(os.path.join(path, 'labelled.json')) as data_file:
            labelled = json.load(data_file)
    # Load unlabelled from disk
    if os.path.exists(os.path.join(path, 'unlabelled.json')):
        with open(os.path.join(path, 'unlabelled.json')) as data_file:
            unlabelled = json.load(data_file)

    for ID in entries.keys():
        if ID not in labelled  and ID not in unlabelled:   # This ID hasn't been labelled
            unlabelled[ID] = None
    
    # Check for manually labelled items
    for ID, label in unlabelled.items():
        if label is not None:
            labelled[ID] = label
    # filter out anything that has been manually set
    unlabelled = {ID: label for ID, label in unlabelled.items() if label is None}  

    # Labelled and unlabelled now up to date
    for id in unlabelled.keys():  # change to iterare through aircraft
        info = aux_info[id]       

        # Proper labelling output
        if id in common.MANUAL_LABELLINGS:
            class_guess = common.MANUAL_LABELLINGS[id]
        else:
            # use first and last 10 entries to determine whether aircraft has landed
            num_include = 25
            alt_threshold = 1500
            comparitor = 0.75
            e_start_above = (np.sum(np.array( entries[id][:num_include])[:,2] > alt_threshold ) / num_include) > comparitor
            e_start_below = (np.sum(np.array( entries[id][:num_include])[:,2] < alt_threshold ) / num_include) > comparitor
            e_end_above = (np.sum(np.array( entries[id][-num_include:])[:,2] > alt_threshold ) / num_include) > comparitor
            e_end_below = (np.sum(np.array( entries[id][-num_include:])[:,2] < alt_threshold ) / num_include) > comparitor

            # run through rules
            if info['OpIcao'] in common.COMMERCIAL_AIRLINE_ICAOS  \
                    and e_start_above \
                    and e_end_below:
                class_guess = 'arrival'

            elif info['OpIcao'] in common.COMMERCIAL_AIRLINE_ICAOS  \
                    and e_start_below \
                    and e_end_above:
                class_guess = 'departure'
                
            elif info['OpIcao'] in common.COMMERCIAL_AIRLINE_ICAOS  \
                    and e_start_above  \
                    and e_end_above: 
                class_guess = 'passthrough'

            else:
                # rules couldn't ID this aircraft
                continue
        
        # Anaomaly output
        # if id in common.ANOAMLIES:
        #     class_guess = 'passthrough' # Just anything really
        # else:
        #     continue

        labelled[id] = LABEL_CLASSES[class_guess]

    # Display stats of loaded labels
    print('Loaded labels:')
    print_label_count(labelled)
    print()

    # Equalise the datasets
    # label_count_min = min([count for count in get_label_count(labelled).values()])
    # label_counts = {v: 0 for v in LABEL_CLASSES.values()}  # use the label number
    # labelled_ret = {}
    
    # for id, label in labelled.items():
    #     label_counts[label] += 1
    #     if label_counts[label] <= label_count_min:
    #         labelled_ret[id] = label
    labelled_ret = labelled


    # Display stats of loaded labels
    print('Equalised labels:')
    print_label_count(labelled_ret)
    print('Total:', len(labelled_ret))
    print()

    # filter out anything that has just been labelled
    unlabelled = {ID: label for ID, label in unlabelled.items() if ID not in labelled_ret} 

    labelled_info = {ID: aux_info[ID] for ID, label in labelled_ret.items()}
    unlabelled_info = {ID: aux_info[ID] for ID, label in unlabelled.items()}

    print('Labelling results:')
    print('Labelled:', len(labelled_ret))
    print('Requiring manual labelling:', len(unlabelled))

    # now save all to disk
    print('Saving to disk... ')
    for label_class in LABEL_CLASSES.values():
        if not os.path.exists(os.path.join(path, 'matrix', str(label_class))):
            os.makedirs(os.path.join(path, 'matrix', str(label_class)))
        if not os.path.exists(os.path.join(path, 'matrix_train', str(label_class))):
            os.makedirs(os.path.join(path, 'matrix_train', str(label_class)))
        if not os.path.exists(os.path.join(path, 'matrix_test', str(label_class))):
            os.makedirs(os.path.join(path, 'matrix_test', str(label_class)))
    
    with open(os.path.join(path, 'labelled.json'), 'w') as outfile:
        json.dump(labelled_ret, outfile, indent=4, sort_keys=True)
    with open(os.path.join(path, 'unlabelled.json'), 'w') as outfile:
        json.dump(unlabelled, outfile, indent=4, sort_keys=True)
    with open(os.path.join(path, 'labelled_info.json'), 'w') as outfile:
        json.dump(labelled_info, outfile, indent=4, sort_keys=True)
    with open(os.path.join(path, 'unlabelled_info.json'), 'w') as outfile:
        json.dump(unlabelled_info, outfile, indent=4, sort_keys=True)
    
    if save_images:
        counter = 1
        print('Saving labelled matrix examples to disk...')

        # For duping only partial tracks
        track_fraction = 1.00  # % of how far along the track should be taken (1=entire track)

        print('-- Dumping entire dataset..')
        # EXPORT IMAGES - ALL DATA
        for id, label_class in labelled_ret.items():
            track_data = entries[id][:int(track_fraction*len(entries[id]))]

            # Calculate actual track length and put only include if it is big enough
            track_len = 0
            for track_item in track_data:
                if common.IMAGE_FORMAT_BOUNDS[0] <= track_item[0] <= common.IMAGE_FORMAT_BOUNDS[2] and\
                   common.IMAGE_FORMAT_BOUNDS[1] <= track_item[1] <= common.IMAGE_FORMAT_BOUNDS[3]:
                    track_len += 1
            if track_len < 100:  # skip tracks that are too short
                continue

            if image_method == 'graphic':
                mat = tools.image_converter.flight_to_image_graphic(common.IMAGE_FORMAT_BOUNDS, 
                                                            common.IMAGE_FORMAT_RESOLUTION, 
                                                            track_data,
                                                            interpolate=False)
            elif image_method == 'tstack':
                mat = tools.image_converter.flight_to_image_tstack(track_data, 
                                                                   common.IMAGE_FORMAT_BOUNDS)
            elif image_method == 'dstack':
                mat = tools.image_converter.flight_to_image_dstack(track_data,
                                                                   common.IMAGE_FORMAT_BOUNDS)

            tools.image_converter.save_image(mat, os.path.join(path, 'matrix', str(label_class), '%s_%d.png'%(id,track_fraction*100)))
        
        # EXPORT IMAGES - TRAINING DATA (pick out random samples)
        print('-- Dumping training dataset..')
        labelled_tmp = labelled_ret.copy()
        num_training_rate = 0.6     # use 60% for training, 40% for testing
        num_training = int(num_training_rate * len(labelled_tmp))
        for _ in range(num_training):
            # Get random element
            id = random.choice(list(labelled_tmp))
            label_class = labelled_tmp[id]
            labelled_tmp.pop(id)
            # Create image
            track_data = entries[id][:int(track_fraction*len(entries[id]))]

            # Calculate actual track length and put only include if it is big enough
            track_len = 0
            for track_item in track_data:
                if common.IMAGE_FORMAT_BOUNDS[0] <= track_item[0] <= common.IMAGE_FORMAT_BOUNDS[2] and\
                   common.IMAGE_FORMAT_BOUNDS[1] <= track_item[1] <= common.IMAGE_FORMAT_BOUNDS[3]:
                    track_len += 1
            if track_len < 100:  # skip tracks that are too short
                continue

            if image_method == 'graphic':
                mat = tools.image_converter.flight_to_image_graphic(common.IMAGE_FORMAT_BOUNDS, 
                                                            common.IMAGE_FORMAT_RESOLUTION, 
                                                            track_data,
                                                            interpolate=False)
            elif image_method == 'tstack':
                mat = tools.image_converter.flight_to_image_tstack(track_data, 
                                                                   common.IMAGE_FORMAT_BOUNDS)
            elif image_method == 'dstack':
                mat = tools.image_converter.flight_to_image_dstack(track_data,
                                                                   common.IMAGE_FORMAT_BOUNDS)
                                                                   
            tools.image_converter.save_image(mat, os.path.join(path, 'matrix_train', str(label_class), '%s_%d.png'%(id,track_fraction*100)))

        # EXPORT IMAGES - TEST DATA (use the remainder left from training)
        print('-- Dumping testing dataset..')
        for _ in range(len(labelled_tmp)):
            # Get random element
            id = random.choice(list(labelled_tmp))
            label_class = labelled_tmp[id]
            labelled_tmp.pop(id)
            # Create image
            track_data = entries[id][:int(track_fraction*len(entries[id]))]

            # Calculate actual track length and put only include if it is big enough
            track_len = 0
            for track_item in track_data:
                if common.IMAGE_FORMAT_BOUNDS[0] <= track_item[0] <= common.IMAGE_FORMAT_BOUNDS[2] and\
                   common.IMAGE_FORMAT_BOUNDS[1] <= track_item[1] <= common.IMAGE_FORMAT_BOUNDS[3]:
                    track_len += 1
            if track_len < 100:  # skip tracks that are too short
                continue

            if image_method == 'graphic':
                mat = tools.image_converter.flight_to_image_graphic(common.IMAGE_FORMAT_BOUNDS, 
                                                            common.IMAGE_FORMAT_RESOLUTION, 
                                                            track_data,
                                                            interpolate=False)
            elif image_method == 'tstack':
                mat = tools.image_converter.flight_to_image_tstack(track_data, 
                                                                   common.IMAGE_FORMAT_BOUNDS)
            elif image_method == 'dstack':
                mat = tools.image_converter.flight_to_image_dstack(track_data,
                                                                   common.IMAGE_FORMAT_BOUNDS)
                                                                   
            tools.image_converter.save_image(mat, os.path.join(path, 'matrix_test', str(label_class), '%s_%d.png'%(id,track_fraction*100)))

    if save_plots:
        counter = 1
        print('Saving flight plots to disk...')
        for id in unlabelled.keys():
            print('Saving figure %d/%d' % (counter, len(unlabelled)))
            counter += 1
            plot_scatter(entries[id], common.AIRPORTS, save_to=os.path.join(path, 'plots', id))

    print('Done')
    
    return labelled_ret


def print_label_count(labelled):
    label_counts = get_label_count(labelled)
    for item in sorted(LABEL_CLASSES.items(), key=operator.itemgetter(1)):
        print('Label {0}: {1}'.format(item[0], label_counts[item[0]]))


def get_label_count(labelled):
    label_count = {k: 0 for k in LABEL_CLASSES.keys()}

    for i in range(1, len(LABEL_CLASSES)+1):
        current_count = sum([v == i for v in labelled.values()])
        label_name = next(key for key, value in LABEL_CLASSES.items() if value == i)
        label_count[label_name] = current_count
    
    return label_count
