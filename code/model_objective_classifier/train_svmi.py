from PIL import Image
import numpy as np
import os
import random
from sklearn import svm
from sklearn.externals import joblib

def load_data(data_dir, set):
    X = []
    y = []

    for i in range(3):
        parent = os.path.join(data_dir, set, str(i+1))
        for filename in os.listdir(parent):
            mat = load_image(os.path.join(parent, filename))

            # Apply transform
            mat = mat.flatten()

            # Save
            X.append(mat.tolist())
            y.append(i)

    # randomize order
    X, y = combined_shuffle(X, y)

    return X, y


def load_image(filename):
    img = Image.open( filename )
    img.load()
    data = np.asarray( img, dtype="int8" )
    return data


def combined_shuffle(a, b):
    combined = list(zip(a, b))
    random.shuffle(combined)

    a[:], b[:] = zip(*combined)
    return a, b


def train(dataset_dir, model_dir):
    # load data
    print('Loading data')
    X_train, y_train = load_data(dataset_dir, 'matrix_train')
    X_test, y_test = load_data(dataset_dir, 'matrix_test')

    print('Training SVM')
    clf = svm.SVC()
    clf.fit(X_train,y_train)

    # Save model
    if not os.path.exists(model_dir):
        os.makedirs(model_dir)
    joblib.dump(clf, os.path.join(model_dir, 'model.dat'))

    print('Testing SVM')
    pred = clf.predict(X_test)
    results = np.array(y_test) == np.array(pred)
    accuracy = np.sum(results)/results.size
    print('Accuracy: ', accuracy)