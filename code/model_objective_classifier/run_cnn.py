import torch
import torchvision
import torchvision.transforms as transforms
import torchvision.datasets as datasets
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import matplotlib.pyplot as plt
import numpy as np
import os
import time
import json
from pathlib import Path

from model_objective_classifier.cnn_data_loader import load_data
import model_objective_classifier.cnn as cnn


def run(dataset_dir, model_dir, architecture):
    # load data
    classes = ('arrival', 'departure', 'passthrough')
    _, testloader = load_data(dataset_dir)

    # Set up GPU
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

    # Set up network
    if 'graphic' in dataset_dir:   # This is a REALLY weak way of doing this...
        classifier_size = 12800
    elif 'tstack' in dataset_dir or 'dstack' in dataset_dir:
        classifier_size = 2048

    if architecture == 'vgg11':
        net = cnn.VGG('VGG11', classifier_size)
    elif architecture == 'vgg16':
        net = cnn.VGG('VGG16', classifier_size)
    elif architecture == 'resnet50':
        net = cnn.ResNet50()
    else:
        print('ERROR: invalid architecture')
        return
    
    
    # Load Network
    net = net.to(device)
    filename = os.path.join(model_dir, 'objective_cnn.dat')
    if not Path(filename).exists():
        print('ERROR: could not find network:', filename)
        return
    net.load_state_dict(torch.load(filename))

    # Load confidence data
    with open(os.path.join(model_dir, 'objective_scores_mean_std (trained).json')) as data_file:
        mean_std = json.load(data_file)

    # Only run test data
    time_start = time.time()
    correct = 0
    total = 0
    # Quickly run through entire dataset
    with torch.no_grad():
        for data in testloader:
            images, labels, paths = data
            images, labels = images.to(device), labels.to(device)

            outputs = net(images)
            _, predicted = torch.max(outputs.data, 1)
            total += labels.size(0)
            correct += (predicted == labels).sum().item()

    time_end = time.time()
    train_time = time_end - time_start

    print('Accuracy of the network on the test images: %.2f %%' % (
        100 * correct / total))

    class_correct = list(0. for i in range(10))
    class_total = list(0. for i in range(10))
    labels_correct, labels_incorrect = {}, {}
    # saves output score value for each category
    output_scores = {    # first index: predicted value, 2nd index: raw scores for each output class
        0: {0: [], 1: [], 2: []},
        1: {0: [], 1: [], 2: []},
        2: {0: [], 1: [], 2: []}
    }

    # Confusion matrix for confidence 
    # Row (1st index):  confident,  not confident
    # Col (2nd index):  actually correct, not correct
    #       actually correct   incorrect
    # confident     a               b
    # not conf.     c               d
    confusion_confidence = np.zeros((2,2))
    # Row (1st index): predicted class
    # Col (2nd index): actual class
    confusion_classification = np.zeros((3,3))

    # Anomaly matrix
    anomaly_count = np.zeros((2,1))  # row 1: # legit, row 2: # anomalies

    with torch.no_grad():
        for data in testloader:
            images, labels, paths = data
            images, labels = images.to(device), labels.to(device)
            outputs = net(images)
            _, predicted = torch.max(outputs, 1)
            c = (predicted == labels).squeeze()
            for i in range(labels.size(0)):
                label = labels[i]
                class_correct[label] += c[i].item()
                class_total[label] += 1
                
                # Save raw scores
                output_scores[label.item()][0].append(outputs[i][0].item())
                output_scores[label.item()][1].append(outputs[i][1].item())
                output_scores[label.item()][2].append(outputs[i][2].item())

                # Get confidence data
                mean_i = torch.Tensor([v for v in mean_std['mean'][str(predicted[i].item())].values()])
                std_i = torch.Tensor([v for v in mean_std['std'][str(predicted[i].item())].values()])
                mean_i = mean_i.to(device)
                std_i = std_i.to(device)

                # Calculate confidence
                d = (outputs[i]-mean_i)/std_i
                # Find indices of highest, 2nd and 3rd highest estimates
                output_1st = outputs[i].topk(1)[1][-1]
                output_2nd = outputs[i].topk(2)[1][-1]
                output_3rd = outputs[i].topk(3)[1][-1]
                zero = torch.Tensor([0]).to(device)

                confidence = torch.Tensor([torch.min(d[output_1st], zero)[0],
                                           torch.max(d[output_2nd], zero)[0],
                                           torch.max(d[output_3rd], zero)[0]])
                confidence = torch.abs(confidence)
                confidence = torch.max(confidence)

                confidence_threshold = 5 # Std deviations
                confident = "Confident" if confidence.item() <= confidence_threshold else "Not confident"

                # Calculate anomaly prediction
                mean_anomaly = 0
                std_anomaly = 1
                d_a = (outputs[i]-mean_anomaly)/std_anomaly
                anomaly = torch.abs(d_a)

                anomaly_threshold = 8
                anomaly_str = "Anomaly" if all(anomaly <= anomaly_threshold) else "Not anomaly"
                if all(anomaly <= anomaly_threshold):
                    anomaly_count[1] += 1
                else:
                    anomaly_count[0] += 1

                # save correct/incorrect predicition label
                if c[i].item() == 0:
                    labels_incorrect[paths[i]] = {}
                    labels_incorrect[paths[i]]['Label'] = classes[label.item()]
                    labels_incorrect[paths[i]]['Predcited'] = classes[predicted[i].item()]
                    labels_incorrect[paths[i]]['score0'] = outputs[i][0].item()
                    labels_incorrect[paths[i]]['score1'] = outputs[i][1].item()
                    labels_incorrect[paths[i]]['score2'] = outputs[i][2].item()
                    labels_incorrect[paths[i]]['confidence'] = confidence.item()
                    labels_incorrect[paths[i]]['confident'] = confident
                    labels_incorrect[paths[i]]['anomaly_str'] = anomaly_str

                    # Update confusion matrices
                    if confidence <= confidence_threshold:
                        confusion_confidence[0][1] += 1 # confident, but wrong
                    else:
                        confusion_confidence[1][1] += 1 # not confident, wrong
                    confusion_classification[int(predicted[i].item())][int(label.item())] += 1
                else:
                    labels_correct[paths[i]] = {}
                    labels_correct[paths[i]]['Label'] = classes[label.item()]
                    labels_correct[paths[i]]['Predcited'] = classes[predicted[i].item()]
                    labels_correct[paths[i]]['score0'] = outputs[i][0].item()
                    labels_correct[paths[i]]['score1'] = outputs[i][1].item()
                    labels_correct[paths[i]]['score2'] = outputs[i][2].item()
                    labels_correct[paths[i]]['confidence'] = confidence.item()
                    labels_correct[paths[i]]['confident'] = confident
                    labels_correct[paths[i]]['anomaly_str'] = anomaly_str

                    # Update confusion matrices
                    if confidence <= confidence_threshold:
                        confusion_confidence[0][0] += 1 # confident, and right
                    else:
                        confusion_confidence[1][0] += 1 # confident, but wrong
                    confusion_classification[int(predicted[i].item())][int(label.item())] += 1
    
    # save labels_correct and labels_incorrect to file
    with open(os.path.join(model_dir, 'objective_errors.json'), 'w') as outfile:
        json.dump(labels_incorrect, outfile, indent=4, sort_keys=True)
    with open(os.path.join(model_dir, 'objective_correct.json'), 'w') as outfile:
        json.dump(labels_correct, outfile, indent=4, sort_keys=True)


    for i in range(3):
        print('Accuracy of %5s : %.2f %%' % (
            classes[i], 100 * class_correct[i] / class_total[i]))
    
    print('Took %.2fs to test %d samples' % (train_time, total))


    # Confusion output
    print('Confusion matrix for confidence:')
    print(confusion_confidence)
    print('Confusion matrix for classification:')
    print(confusion_classification)

    # Anomaly output
    print('Anomalies:')
    print(' (1st row: legit, 2nd row: anomalies')
    print(anomaly_count)

    # Chart the score outputs of each category
    output_scores = { 
        0: {0: np.array(output_scores[0][0]), 1: np.array(output_scores[0][1]), 2: np.array(output_scores[0][2])},
        1: {0: np.array(output_scores[1][0]), 1: np.array(output_scores[1][1]), 2: np.array(output_scores[1][2])},
        2: {0: np.array(output_scores[2][0]), 1: np.array(output_scores[2][1]), 2: np.array(output_scores[2][2])},
    }

    output_scores_mean = {
        0: {0: np.mean(output_scores[0][0]), 1: np.mean(output_scores[0][1]), 2: np.mean(output_scores[0][2])},
        1: {0: np.mean(output_scores[1][0]), 1: np.mean(output_scores[1][1]), 2: np.mean(output_scores[1][2])},
        2: {0: np.mean(output_scores[2][0]), 1: np.mean(output_scores[2][1]), 2: np.mean(output_scores[2][2])},
    }

    output_scores_std = {
        0: {0: np.std(output_scores[0][0]), 1: np.std(output_scores[0][1]), 2: np.std(output_scores[0][2])},
        1: {0: np.std(output_scores[1][0]), 1: np.std(output_scores[1][1]), 2: np.std(output_scores[1][2])},
        2: {0: np.std(output_scores[2][0]), 1: np.std(output_scores[2][1]), 2: np.std(output_scores[2][2])},
    }

    # Don't save to disk in the runtime version - want to use trained values
    # save mean and stddev values
    output_scores_mean_std = {
        "mean": output_scores_mean,
        "std": output_scores_std
    }
    with open(os.path.join(model_dir, 'objective_scores_mean_std.json'), 'w') as outfile:
        json.dump(output_scores_mean_std, outfile, indent=4, sort_keys=True)

    chart_labels = ['Arrival', 'Departure', 'Passing through']
    x_pos = np.arange(len(chart_labels))
    for graphing in range(3):
        CTEs = [output_scores_mean[graphing][0],
                output_scores_mean[graphing][1],
                output_scores_mean[graphing][2]]
        error = [output_scores_std[graphing][0],
                output_scores_std[graphing][1],
                output_scores_std[graphing][2]]
        
        fig, ax = plt.subplots()
        ax.bar(x_pos, CTEs, yerr=error, align='center', alpha=0.5, ecolor='black', capsize=10)
        ax.set_ylabel('Raw output score')
        ax.set_xticks(x_pos)
        ax.set_xticklabels(chart_labels)
        ax.set_title('Raw output of CNN for %s label' % chart_labels[graphing])
        ax.yaxis.grid(True)

        plt.tight_layout()
        plt.savefig('outputs/output_scores_%s.png' % chart_labels[graphing])
        plt.show()



