import torch
import torchvision
import torchvision.transforms as transforms
import torchvision.datasets as datasets
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import matplotlib.pyplot as plt
import numpy as np
import os
import time
import json
from pathlib import Path

from model_objective_classifier.cnn_data_loader import load_data
import model_objective_classifier.cnn as cnn


def train(dataset_dir, model_dir, architecture):
    # load data
    classes = ('arrival', 'departure', 'passthrough')
    trainloader, testloader = load_data(dataset_dir)

    # Set up GPU
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

    # Set up network
    if 'graphic' in dataset_dir:   # This is a REALLY weak way of doing this...
        classifier_size = 12800
    elif 'tstack' in dataset_dir or 'dstack' in dataset_dir:
        classifier_size = 2048

    if architecture == 'vgg11':
        net = cnn.VGG('VGG11', classifier_size)
    elif architecture == 'vgg16':
        net = cnn.VGG('VGG16', classifier_size)
    elif architecture == 'resnet50':
        net = cnn.ResNet50()
    else:
        print('ERROR: invalid architecture')
        return
    
    net = net.to(device)
    if not os.path.exists(model_dir):
        os.makedirs(model_dir)
    filename = os.path.join(model_dir, 'objective_cnn.dat')

    # 3. Define a Loss function and optimizer
    criterion = nn.CrossEntropyLoss()
    optimizer = optim.SGD(net.parameters(), lr=0.001, momentum=0.9)

    # 4. Train the network
    time_start = time.time()
    loss_record = []
    for epoch in range(200):  # loop over the dataset multiple times
        # print('Epoch', epoch)

        # Learning rate adjustments
        if epoch == 0:
            print('Learning rate set to 0.01')
            lr = 0.01
            optimizer = optim.SGD(net.parameters(), lr=lr, momentum=0.9)
        elif epoch == 149:
            print('Learning rate set to 0.001')
            lr = 0.001
            optimizer = optim.SGD(net.parameters(), lr=lr, momentum=0.9)
        elif epoch == 249:
            print('Learning rate set to 0.0001')
            lr = 0.0001
            optimizer = optim.SGD(net.parameters(), lr=lr, momentum=0.9)

        running_loss = 0.0
        running_count = 0
        for i, data in enumerate(trainloader, 0):
            # get the inputs
            inputs, labels, paths = data
            inputs, labels = inputs.to(device), labels.to(device)

            # zero the parameter gradients
            optimizer.zero_grad()

            # forward + backward + optimize
            outputs = net(inputs)
            outputs = outputs.to(device)
            loss = criterion(outputs, labels)
            loss.backward()
            optimizer.step()

            # print statistics
            running_loss += loss.item()
            running_count += 1
            if i % 2000 == 1999:    # print every 2000 mini-batches
                print('[%d, %5d] loss: %.3f' %
                    (epoch + 1, i + 1, running_loss / 2000))
                running_loss = 0.0
        print('Epoch %d loss: %.6f' % (epoch+1, running_loss/running_count))

        loss_record.append([epoch+1, lr, running_loss/running_count])

    time_end = time.time()
    train_time = time_end - time_start
    print('Finished Training')
    print('Training took %.2fs (%.2f min)' % (train_time, train_time/60))

    # Save CNN graph to disk
    print('Saving CNN to disk -', filename)
    torch.save(net.state_dict(), filename)

    # save epoch/loss record to disk
    np.savetxt(os.path.join(model_dir, 'objective_loss.csv'), np.array(loss_record), delimiter=',', 
                header='epoch,lr,loss')

    # 5. Test the network on the test data
    dataiter = iter(testloader)
    images, labels, paths = dataiter.next()

    # print images
    # imshow(torchvision.utils.make_grid(images))
    print('GroundTruth: ', ' '.join('%5s' % classes[labels[j]] for j in range(4)))

    images = images.to(device)
    outputs = net(images)

    _, predicted = torch.max(outputs, 1)
    
    print('Predicted: ', ' '.join('%5s' % classes[predicted[j]]
                                for j in range(4)))

    correct = 0
    total = 0
    with torch.no_grad():
        for data in testloader:
            images, labels, paths = data
            images, labels = images.to(device), labels.to(device)

            outputs = net(images)
            _, predicted = torch.max(outputs.data, 1)
            total += labels.size(0)
            correct += (predicted == labels).sum().item()

    print('Accuracy of the network on the test images: %d %%' % (
        100 * correct / total))

    class_correct = list(0. for i in range(10))
    class_total = list(0. for i in range(10))
    labels_correct, labels_incorrect = {}, {}
    with torch.no_grad():
        for data in testloader:
            images, labels, paths = data
            images, labels = images.to(device), labels.to(device)
            outputs = net(images)
            _, predicted = torch.max(outputs, 1)
            c = (predicted == labels).squeeze()
            for i in range(labels.size(0)):
                label = labels[i]
                class_correct[label] += c[i].item()
                class_total[label] += 1

                # save to file if incorrect
                if c[i] == 0:
                    labels_incorrect[paths[i]] = {}
                    labels_incorrect[paths[i]]['Label'] = classes[label.item()]
                    labels_incorrect[paths[i]]['Predcited'] = classes[predicted[i].item()]
                else:
                    labels_correct[paths[i]] = {}
                    labels_correct[paths[i]]['Label'] = classes[label.item()]
                    labels_correct[paths[i]]['Predcited'] = classes[predicted[i].item()]

    
    # save labels_incorrect to file
    with open(os.path.join(model_dir, 'objective_errors.json'), 'w') as outfile:
        json.dump(labels_incorrect, outfile, indent=4, sort_keys=True)
    with open(os.path.join(model_dir, 'objective_correct.json'), 'w') as outfile:
        json.dump(labels_correct, outfile, indent=4, sort_keys=True)


    for i in range(3):
        print('Accuracy of %5s : %2d %%' % (
            classes[i], 100 * class_correct[i] / class_total[i]))
    
    print('----- END OF CNN -----')