#!/usr/bin/env python3

import sys
import os

from tools.load_adsb import load
import tools.common as common
import model_objective_classifier.objective_labeller
import model_objective_classifier.train_cnn
import model_objective_classifier.run_cnn
import model_objective_classifier.train_rnn
import model_objective_classifier.run_rnn
import model_objective_classifier.train_svmi
import model_objective_classifier.run_svmi
import model_objective_classifier.train_svmd
import model_objective_classifier.run_svmd
import model_pos_clustering.pos_clustering_train


def run(mode, model, dataset, modelstore):
    modelDir = os.path.join('./models/', modelstore)

    if mode == 'train':
        if model == 'trajectory':
            entries, aux_info = load(dataset)
            # Only load a select number of aircraft right now (to speed up plotting)
            ids = [
               '7C6D23_TGG461', 
               '7C6C57_VOZ231',
               '7C6DE0_QFA693',
               '7C6D27_VOZ229',
               '7C6D95_QFA683',
               'C822C4_TFR36',
               '7C5BEA_VOZ9575',
               '7C7A3C_VOZ211',
               '7C6D32_VOZ219',
               '7C6D7E_JST772',
               '7C6D90_QFA697',
               '7C6D23_TGG471',
               '7C7A3D_VOZ213',
               '7C7802_QFA691',
               '7C4320_JTE7458'
            ]
            input_entries = {id: entries[id] for id in ids}
            print('Running trajectory on %d entries' % len(input_entries))
            model_pos_clustering.pos_clustering_train.train(input_entries, common.AIRPORTS)
            
        elif model == 'objective_svmd':
            model_objective_classifier.train_svmd.train(dataset, modelDir)
        elif model == 'objective_svmi':
            model_objective_classifier.train_svmi.train(dataset, modelDir)
        elif model == 'objective_cnn_vgg11':
            model_objective_classifier.train_cnn.train(dataset, modelDir, 'vgg11')
        elif model == 'objective_cnn_vgg16':
            model_objective_classifier.train_cnn.train(dataset, modelDir, 'vgg16')
        elif model == 'objective_cnn_resnet50':
            model_objective_classifier.train_cnn.train(dataset, modelDir, 'resnet50')
        elif model == 'objective_rnn':
            model_objective_classifier.train_rnn.train(dataset, modelDir)

        else:
            print('INVALID MODEL')
            printHelp()
    
    elif mode == 'test':
        if model == 'trajectory':
            pass
        elif model == 'objective_svmd':
            model_objective_classifier.run_svmd.run(dataset, modelDir)
        elif model == 'objective_svmi':
            model_objective_classifier.run_svmi.run(dataset, modelDir)
        elif model == 'objective_cnn_vgg11':
            model_objective_classifier.run_cnn.run(dataset, modelDir, 'vgg11')
        elif model == 'objective_cnn_vgg16':
            model_objective_classifier.run_cnn.run(dataset, modelDir, 'vgg16')
        elif model == 'objective_cnn_resnet50':
            model_objective_classifier.run_cnn.run(dataset, modelDir, 'resnet50')
        elif model == 'objective_rnn':
            model_objective_classifier.run_rnn.run(dataset, modelDir)

        else:
            print('INVALID MODEL')
            printHelp()
    
    elif mode == 'genlabels':
        if model == 'objective_graphic':
            entries, aux_info = load(dataset)
            model_objective_classifier.objective_labeller.create_labels(entries, aux_info, modelstore, 'graphic')

        elif model == 'objective_tstack':
            entries, aux_info = load(dataset)
            model_objective_classifier.objective_labeller.create_labels(entries, aux_info, modelstore, 'tstack')

        elif model == 'objective_dstack':
            entries, aux_info = load(dataset)
            model_objective_classifier.objective_labeller.create_labels(entries, aux_info, modelstore, 'dstack')
        
        else:
            print('INVALID MODEL')
            printHelp()
    
    else:
        print('INVALID MODE')
        printHelp()


def printHelp():
    helpMsg = """Trains and runs models.
Usage: launcher.py mode model dataset modelstore

Modes:
    train           Trains the model and saves to 'modelstore'
    test            Runs a trained model saved as 'modelstore'
    genlabels       Only valid for model=objective. Generates labelled data from 'dataset', stores in 'modelstore'.

Models:
    For mode=train or test
    trajectory              Trajectory clustering model
    objective_svmd          Objective classifier model - SVM (direct data input)
    objective_svmi          Objective classifier model - SVM (image data input)
    objective_cnn_vgg11     Objective classifier model - CNN (VGG11)
    objective_cnn_vgg16     Objective classifier model - CNN (VGG16)
    objective_cnn_resnet50  Objective classifier model - CNN (50 layer ResNet)
    objective_rnn           Objective classifier model - RNN (Vanilla)

    For mode=genlabels
    objective_graphic       Image-like flight track.
    objective_tstack        More efficient storage. Sorted in time.
    objective_dstack        Like tstack, but sorted from distance to airport

dataset:
    Input dataset to use.
    For image inputs (like CNN), use:
        dataset="./Data/labelled/datasetname/"
    For models requiring raw data, use:
        dataset="./Data/ADSBExchange/processed/" 
        If a folder is supplied, JSON files from specific dates will be loaded
        If a file is specified, that file will be loaded

modelstore:
    if mode=train or test:
        Load or save the model from "./models/{modelstore}/"
    if mode=genlabels:
        Save the labels to "./Data/labels_objective/{modelstore}/" """
    print(helpMsg)


if __name__ == '__main__':
    if len(sys.argv) == 5:
        run(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4])
    else:
        printHelp()
