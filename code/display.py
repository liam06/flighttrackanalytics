#!/usr/bin/env python3

import sys
from pprint import pprint
import numpy as np
import matplotlib.pyplot as plt

from tools.load_adsb import load
import tools.common as common
import tools.kalman as kalman
import tools.plotting
import tools.image_converter


def display(dataset, Icao_Call):
    entries, aux_info = load(dataset)
    if Icao_Call not in entries:
            print('Invalid Icao_Call')

    # Kalman filtering --------------------------------
    entry_array = np.array(entries[Icao_Call])
    xhat, res = kalman.run_kalman(entry_array)
    entries_2 = xhat.tolist()  # convert back to Python list

    print('res shape:', res.shape)
    res_norm = np.linalg.norm(res, axis=1)
    print('res_norm shape:', res_norm.shape)

    plt.figure()
    plt.plot(res_norm,'k+')
    plt.legend()
    plt.xlabel('Iteration')
    plt.ylabel('Residual')
    plt.show()

    for entry in entries[Icao_Call]:
        print(entry)
    print('-------') 
    # -------------------------------------------------

    print('Flight info:')
    pprint(aux_info[Icao_Call])
    tools.plotting.plot_scatter(entries[Icao_Call], common.AIRPORTS, S=entries_2)

    # also save to file
    flight_array = entries[Icao_Call]
    flight_mat = tools.image_converter.flight_to_image_graphic(common.IMAGE_FORMAT_BOUNDS, common.IMAGE_FORMAT_RESOLUTION, flight_array, interpolate=False)
    tools.image_converter.save_image(flight_mat, 'outputs/display_graphic.png')

    flight_mat = tools.image_converter.flight_to_image_tstack(flight_array, common.IMAGE_FORMAT_BOUNDS)
    tools.image_converter.save_image(flight_mat, 'outputs/display_tstack.png')

    flight_mat = tools.image_converter.flight_to_image_dstack(flight_array, common.IMAGE_FORMAT_BOUNDS)
    tools.image_converter.save_image(flight_mat, 'outputs/display_dstack.png')


if __name__ == '__main__':
    if len(sys.argv) == 3:
        display(sys.argv[1], sys.argv[2]) 
    else:
        print('Usage: display.py dataset Icao_Call')
        print(' dataset: E.g. ./Data/ADSBExchange/processed/')
