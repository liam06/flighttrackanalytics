#!/usr/bin/env python3

# Just combines the JSON dictionaries

import sys
import os
import json

from tools.load_adsb import load
import tools.common as common


def main(source_dir, outputfile):
    d = {}

    # Load
    for date in common.DATA_DATES:
        file_path = os.path.join(source_dir, '%s.json' % date) 
        print('Loading %s ...' % file_path)

        with open(file_path) as data_file:
            data_n = json.load(data_file)
        
        d.update(data_n)  # merge in the new elements
    
    # Save
    print('Saving to %s ...' % outputfile)
    with open(outputfile, 'w') as outfile:
        json.dump(d, outfile, indent=4, sort_keys=True)


if __name__ == '__main__':
    if len(sys.argv) != 3:
        print('Usage: combine_json.py source_dir outputfile.json')
        exit()

    main(sys.argv[1], sys.argv[2])
