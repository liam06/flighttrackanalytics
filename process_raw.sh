#!/usr/bin/env bash

DATASET_NAME=Sydney_Far

python3 ./code/tools/load_adsb_raw.py /media/liam/New\ Volume/Honours\ Data/ADS-B\ Exchange/data/2018-03-05/ ./Data/ADSBExchange/processed/$DATASET_NAME/
python3 ./code/tools/load_adsb_raw.py /media/liam/New\ Volume/Honours\ Data/ADS-B\ Exchange/data/2018-03-06/ ./Data/ADSBExchange/processed/$DATASET_NAME/
python3 ./code/tools/load_adsb_raw.py /media/liam/New\ Volume/Honours\ Data/ADS-B\ Exchange/data/2018-03-07/ ./Data/ADSBExchange/processed/$DATASET_NAME/
python3 ./code/tools/load_adsb_raw.py /media/liam/New\ Volume/Honours\ Data/ADS-B\ Exchange/data/2018-03-08/ ./Data/ADSBExchange/processed/$DATASET_NAME/
python3 ./code/tools/load_adsb_raw.py /media/liam/New\ Volume/Honours\ Data/ADS-B\ Exchange/data/2018-03-09/ ./Data/ADSBExchange/processed/$DATASET_NAME/
python3 ./code/tools/load_adsb_raw.py /media/liam/New\ Volume/Honours\ Data/ADS-B\ Exchange/data/2018-03-10/ ./Data/ADSBExchange/processed/$DATASET_NAME/
python3 ./code/tools/load_adsb_raw.py /media/liam/New\ Volume/Honours\ Data/ADS-B\ Exchange/data/2018-03-11/ ./Data/ADSBExchange/processed/$DATASET_NAME/
python3 ./code/tools/load_adsb_raw.py /media/liam/New\ Volume/Honours\ Data/ADS-B\ Exchange/data/2018-03-12/ ./Data/ADSBExchange/processed/$DATASET_NAME/
python3 ./code/tools/load_adsb_raw.py /media/liam/New\ Volume/Honours\ Data/ADS-B\ Exchange/data/2018-03-13/ ./Data/ADSBExchange/processed/$DATASET_NAME/
python3 ./code/tools/load_adsb_raw.py /media/liam/New\ Volume/Honours\ Data/ADS-B\ Exchange/data/2018-03-14/ ./Data/ADSBExchange/processed/$DATASET_NAME/
python3 ./code/tools/load_adsb_raw.py /media/liam/New\ Volume/Honours\ Data/ADS-B\ Exchange/data/2018-03-15/ ./Data/ADSBExchange/processed/$DATASET_NAME/

